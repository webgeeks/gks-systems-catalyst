use strict;
use warnings;
use Test::More;


use Catalyst::Test 'GKS';
use GKS::Controller::Books;

#ok( request('/books')->is_success, 'Request should succeed' );
ok( request('/books')->is_redirect, 'Request should succeed' );
done_testing();
