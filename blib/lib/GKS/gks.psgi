use strict;
use warnings;

use GKS;

my $app = GKS->apply_default_middlewares(GKS->psgi_app);
$app;

