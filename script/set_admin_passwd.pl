#!/usr/bin/env perl

use strict;
use warnings;
use lib 'lib';

BEGIN { $ENV{CATALYST_DEBUG} = 0 }

use GKS;
use DateTime;

my $admin = GKS->model('DB::User')->search({ username => 'webgeeks' })
    ->single;

$admin->update({ password => 'admin' });