package GKS;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.90;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
    -Debug
    ConfigLoader
    DateTime
    Static::Simple

    StackTrace

    Authentication
    Authorization::Roles

    Session
    Session::State::Cookie
    Session::Store::Memcached
    
    SmartURI

    StatusMessage
    
    Unicode

/;

extends 'Catalyst';

our $VERSION = '0.04';

# Configure the application.
#
# Note that settings in gks.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
    name => 'GKS Systems',
    # Disable deprecated behavior needed by old applications
    disable_component_resolution_regex_fallback => 1,
    enable_catalyst_header => 1, # Send X-Catalyst header
    encoding => 'UTF-8',
    default_view => 'HTML',
);

## Configure SimpleDB Authentication
#__PACKAGE__->config(
#    'Plugin::Authentication' => {
#        default => {
#            class           => 'SimpleDB',
#            user_model      => 'DB::User',
#            password_type   => 'self_check',
#        },
#    },
#);

# Configure Authentication
# Additional realms: FaceBook, Google+, etc.
__PACKAGE__->config('Plugin::Authentication' => {
    default_realm => 'localdb',
    realms => {
        localdb => {
            credential => {
               class          => 'Password',
               password_field => 'password',
               password_type  => 'self_check'
            },
            store => {
                class => 'DBIx::Class',
                user_model => 'DB::User',
                role_relation => 'roles',
                role_field => 'role',
            }
        },
        nopassword => {
        	credential => {
               class 	=> 'NoPassword'
            },
            store => {
                class => 'DBIx::Class',
                user_model => 'DB::User',
                role_relation => 'roles',
                role_field => 'role',
            }
        }
    }
    
});

# Configure Sessions
  __PACKAGE__->config(
    session => {
        expires => 14400,
        flash_to_stash => 1,
        cookie_secure => 2,
    },
  );
  
  
__PACKAGE__->config(
	'View::HTML' => {
    # Change default TT extension
    #TEMPLATE_EXTENSION => '.tt2',
    # Set the location for TT files
    INCLUDE_PATH => [
            __PACKAGE__->path_to( 'root' ),
        	],
    # Set to 1 for detailed timer stats in your HTML as comments
    TIMER		=> 1,
    render_die 	=> 1,
    ENCODING	=> 'utf-8',
    # This is your wrapper template located in the 'root/src'
    WRAPPER 	=> 'wrapper.tt2',
    
    # Production Settings
    
    VARIABLES => {
        version => 3.14,
        release => 'Sahara',
        statuscodes => { #CSS Classes for Status Codes
        			1 => 'label-primary',
        			2 => 'label-info',
        			3 => 'label-info',
        			4 => 'label-warning',
        			5 => '',
        			6 => 'label-danger',
        	}
    },
    expose_methods => [qw/ statuscode_classes /],
    # /  
    }
);


# Configure EMail
 __PACKAGE__->config(
        'View::Email' => {
            # Where to look in the stash for the email information.
            # 'email' is the default, so you don't have to specify it.
            stash_key => 'email',
            # Define the defaults for the mail
            default => {
                # Defines the default content type (mime type). Mandatory
                content_type => 'text/plain',
                # Defines the default charset for every MIME part with the 
                # content type text.
                # According to RFC2049 a MIME part without a charset should
                # be treated as US-ASCII by the mail client.
                # If the charset is not set it won't be set for all MIME parts
                # without an overridden one.
                # Default: none
                charset => 'utf-8'
            },
            # Setup how to send the email
            # all those options are passed directly to Email::Sender::Simple
            sender => {
                # if mailer doesn't start with Email::Sender::Simple::Transport::,
                # then this is prepended.
                mailer => 'SMTP',
                # mailer_args is passed directly into Email::Sender::Simple 
                mailer_args => {
                    host     => 'localhost', # defaults to localhost
                    #sasl_username => 'sasl_username',
                    #sasl_password => 'sasl_password',
            }
          }
        }
);

__PACKAGE__->config(
	'View::Email::Template' => {
	    stash_key       => 'email',
	    template_prefix => ''
	}
);



# Start the application
__PACKAGE__->setup();


=head1 NAME

GKS - Catalyst based application

=head1 SYNOPSIS

    script/gks_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<GKS::Controller::Root>, L<Catalyst>

=head1 AUTHOR

The Web Geeks,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
