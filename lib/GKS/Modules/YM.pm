package GKS::Modules::YM;
use Data::UUID;

require      Exporter;

our @ISA       = qw(Exporter);
our @EXPORT    = qw(gen_ym_callid);




#Central Module for generating UUID's
# Data::UUID seems to generate better (less predictable) UUID's than MySQL
# But if we need to change, it is all in one place.

sub gen_ym_callid() {
    my $ug = new Data::UUID;
    return substr($ug->create_hex(), 0, 20);
}
    
1;