package GKS::Form::Validation;
use Moose::Role;


 sub is_strong_password {
    my $self = shift;
    $self->add_error('Password must contain at least one uppercase, one lowercase and one number.') unless (
    	$self->value =~ /[a-z]/ &&
    	$self->value =~ /[A-Z]/ &&
    	$self->value =~ /\d/);
}
# __PACKAGE__->meta->make_immutable;
1;