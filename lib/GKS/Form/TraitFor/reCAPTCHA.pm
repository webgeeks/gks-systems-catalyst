package GKS::Form::TraitFor::reCAPTCHA;
# ABSTRACT: generate and validate captchas

use HTML::FormHandler::Moose::Role;

has_field 'recaptcha' => ( type => 'reCAPTCHA', label => 'Verification' );


sub get_captcha {
#    my $self = shift;
#    return unless $self->ctx;
#    my $captcha;
#    $captcha = $self->ctx->session->{captcha};
#    return $captcha;
}


sub set_captcha {
#    my ( $self, $captcha ) = @_;
#    return unless $self->ctx;
#    $self->ctx->session( captcha => $captcha );
}


sub captcha_image_url {
    #return '/captcha/image';
}

use namespace::autoclean;
1;