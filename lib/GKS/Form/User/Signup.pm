package GKS::Form::User::Signup;
use HTML::FormHandler::Moose;
use HTML::FormHandler::Types ('NotAllDigits');
#use Captcha::reCAPTCHA;

extends 'HTML::FormHandler';
with 'GKS::Form::Validation';
#with 'HTML::FormHandler::Widget::Theme::Bootstrap3';
#with 'HTML::FormHandlerX::Field::reCAPTCHA';
#with 'GKS::Form::TraitFor::reCAPTCHA';
#with 'GKS::Form::Field::reCAPTCHA';

#with 'GKS::Form::Theme::Bootstrap';

	#has '+widget_wrapper' => ( default => 'Bootstrap3' );
	#has '+widget_name_space' => ( default => sub { ['GKS::Form::Widget' ] } );
	#has '+field_name_space' => ( default => 'GKS::Form::Field' );
# webgks: you need to provide an 'item' or set 'item_class' to your row source name:
#	has '+item_class' => ( default => 'Profile' );
	
	has_field 'email_address' => (label => 'Email', required => 1);
 	
 	has_field 'new_password' => ( type => 'Password', minlength => 8, required => 1,
   								apply => [ NotAllDigits ], validate_method => \&is_strong_password );
   	has_field 'confirm_password' => ( type => 'PasswordConf', label => 'Confirm Password',
   									password_field => 'new_password', required => 1,
   									minlength => 8,
   									messages => { pass_conf_not_matched => 'Passwords do not match.' },);

	#has 'status' => ( isa => 'Maybe[Str]', is => 'rw');
	#has_field 'business_name' => (label => 'Company');
	has_field 'first_name' => (label => 'First Name',);
	has_field 'last_name' => (label => 'Last Name',);
	
	has_field 'recaptcha' => (type => 'reCAPTCHA', do_label => 0,
		required=>1,
		public_key => GKS->config->{recaptcha}->{public_key},
		private_key => GKS->config->{recaptcha}->{private_key},
		recaptcha_options => {
			theme => 'white',
			},
		recaptcha_message => "Captcha response does not match.",
		);
	
#	has_field 'CAPTCHA' => (
#        type=>'Hidden', 
#        widget => 'reCAPTCHA',
#        public_key=>'[YOUR PUBLIC KEY]',
#        default => '6Ldr0_USAAAAALHoGjKKQSND0rSIZl13E4c56c5z',
#        value => 'FOOBAR!!!',
#        foo => 'FooBar',
#        #private_key=>'[YOUR PRIVATE KEY]',
#        #recaptcha_message => "You're failed to prove your Humanity!",
#        #required=>1,
#    );
#
#	has_field 'recaptcha' => (type => 'Hidden', noupdate => 1, tags => {before_element => \&set_recaptcha_theme, after_element => \&gen_recaptcha });
	
	#has_field 'users.user_uuid' => ( type => 'Hidden' );
	#has_field 'users.username' => ( type => 'Hidden' );
	#has_field 'account_name' => ( type => 'Hidden');
	#has_field 'users.is_primary' => ( type => 'Hidden' );
	#has_field 'users.created_by' => (  type => 'Hidden'  );
	#has_field 'profiles.status' => (type => 'Hidden', default => '1');
	#has_field 'profiles.type' => (type => 'Hidden', default => '10');
	#has 'account_uuid' => ( isa => 'Str', is => 'rw' );
	#has_field 'account_uuid' => ( type => 'Hidden' );
	#has_field 'users.status' =>(type => 'Hidden', default => '1');
	

 has_field 'form_actions' => ( type => 'Compound', noupdate => 1, do_wrapper => 1, );
    has_field 'form_actions.save' => ( widget_wrapper => 'None', widget => 'ButtonTag', type => 'Submit',
        value => 'Create Account', element_attr => { style => '' }, element_class => 'btn btn-primary');
    has_field 'form_actions.cancel' => ( widget_wrapper => 'None', widget => 'ButtonTag', type => 'Reset',
        value => 'Cancel', element_class => 'btn btn-default');

# has_field 'form_actions' => ( type => 'Compound', wrapper_class => 'plain', noupdate => 1 );
#    has_field 'form_actions.save' => ( widget => 'ButtonTag', type => 'Submit',
#        value => 'Save', element_attr => { style => 'width: 100px;' },);
#    has_field 'form_actions.cancel' => ( widget => 'ButtonTag', type => 'Reset',
#        value => 'Cancel',);


#sub set_recaptcha_theme() {
#	my $output = qq{
#		<script type="text/javascript">
#		 var RecaptchaOptions = {
#		    theme : 'white'
#		 };
#		 </script>		
#	}
#	
#}
#
#sub gen_recaptcha {
#
#	my $c = Captcha::reCAPTCHA->new;
#	
#	my $output = $c->get_html("6Ldr0_USAAAAALHoGjKKQSND0rSIZl13E4c56c5z");
#	
#	return $output;
#}

#	sub default_users_user_uuid { 
#		my $self = shift; 
#		$self->{users}->{user_uuid};
#		#print Dumper($self->{users}->{user_uuid});
#	}
	#sub default_accounts_status {'1'}


#	before 'update_model' => sub {
#       my $self = shift;
#       $self->item->status('1');
#       #$self->item->username($self->username);
#       #$self->item->{accounts}{status}('1');
#       #print Dumper($self->field('users.0.email_address'));
#       #$self->field('users.0.username') = $self->field('users.0.email_address');
#       #print Dumper($self->field('users.0.user_uuid'));
#       #$accounts->status('1');
#       $self->item->account_uuid( $self->account_uuid );
#    };
#    
#    sub update_model {
#        my $self = shift;
#        my $values = $self->values;
#        print Dumper($values->{users});
#        $values->{users}->['0']->{username} = $values->{users}->['0']->{email_address};
#        $self->_set_value($values);        
#        $self->next::method;
#    }


__PACKAGE__->meta->make_immutable;

1;