package GKS::Form::User::NewProfile;
use HTML::FormHandler::Moose;
use HTML::FormHandler::Types ('NotAllDigits');
use Locale::SubCountry;
#extends 'HTML::FormHandler::Model::DBIC';
extends 'HTML::FormHandler';

has '+widget_wrapper' => ( default => 'Bootstrap3' );

#with 'HTML::FormHandler::Widget::Theme::Bootstrap3';

   #has '+widget_name_space' => ( default => sub { ['GKS::Form::Widget' ] } );
   #has '+field_name_space' => ( default => sub { ['GKS::Form::Field' ] } );
   #has_field 'status' => ( type => 'Select', label => 'Status', empty_select => '-- Choose --', sort_column=>'sort_order', active_column => 'active',);
   #has 'created' => ( isa => 'DateTime', is => 'rw' );
   #has 'account_uuid' => ( isa => 'Str', is => 'rw' );
   
      #has_field 'type' => (label => 'Profile Type', type => 'Select', options_method => \&options_profiles );
   has_field 'first_name' => (label => 'First Name');
   has_field 'last_name' => (label => 'Last Name');
   has_field 'email_address' => (label => 'Email', required => 1, type => 'Email', wrapper_class => 'required');
   has_field 'business_name' => (label => 'Company');
   has_field 'url' => (label => 'URL');
   has_field 'address1' => (label => 'Address', );
   has_field 'address2' => (label => 'Address Line 2');
   has_field 'city';
   has_field 'state' => ( type => 'Select', empty_select => '---Choose---', options_method => \&options_state );
   has_field 'postalcode' => (label => 'Zip' );
   has_field 'phone_mobile' => (label => 'Phone: Mobile' );
   has_field 'phone_work' => (label => 'Phone: Work' );
   has_field 'phone_fax' => (label => 'Phone: Fax' );
   has_field 'phone_other' => (label => 'Phone: Other' );
   has_field 'type' => (label => 'Category', type => 'Select', options_method => \&options_categories );

   sub options_state {
   		##Needs to be dynamic based on Country...
   		my @selections;
	    my $country_code = 'US';
	    my $US = new Locale::SubCountry($country_code);
	    
	    if (  $US->has_sub_countries )
	        {
	            my @state_names  = $US->all_full_names;
	            my @code_names   = $US->all_codes;
	            my %states_keyed_by_code  = $US->code_full_name_hash;
	            my %states_keyed_by_name  = $US->full_name_code_hash;
	
	            foreach my $code ( sort keys %states_keyed_by_code )
	            {
	               #printf("%-3s : %s\n",$code,$states_keyed_by_code{$code});
	                push @selections, { value => $code, label => $code . '-' . $states_keyed_by_code{$code}, note => 'Note' };
	            }
	        }
        return @selections;
     }
     
  sub options_categories {
  	#my ($self, $c)=@_;
  	 #$c->log->debug('UserID: ' . $c->user->id);
	return (
           
       );
	}


 #sub build_form_element_class { ['form-vertical'] }
 
#    before 'update_model' => sub {
#       my $self = shift;
#       $self->item->created( $self->created );
#    };

#sub build_form_tags {
#        {   wrapper_tag => 'div',
#            before => '',
#            after => '</div>',
#        }
#    }


 __PACKAGE__->meta->make_immutable;

 1;