package GKS::Form::User::Login;
use HTML::FormHandler::Moose;
extends 'HTML::FormHandler';
#with 'HTML::FormHandlerX::Form::JQueryValidator';
#with 'GKS::Form::Theme::Bootstrap3';
#with 'GKS::Form::Theme::BootstrapFormMessages';

   #has '+widget_name_space' => ( default => sub { ['GKS::Form::Widget' ] } );
   has '+widget_wrapper' => ( default => 'Bootstrap3' );
   has_field 'redirect_after_login' => (type => 'Hidden', is => 'rw');
   has_field 'username' => ( do_label => 0, element_attr => { placeholder => 'Username' }, required => 1);
   has_field 'password' => ( type => 'Password', do_label => 0, required => 1, element_attr => { placeholder => 'Password' },);
   #has_field validation_json => ( type => 'Hidden',  element_attr => { disabled => 'disabled' } );
   
   has_field 'submit'   => (  type => 'Submit', widget => 'ButtonTag',
    						value => 'Sign In', element_class => 'btn btn-small btn-primary btn-block',
    						do_label => 0, );
    						
	

	#sub default_validation_json { shift->as_escaped_json }
	

   #has '+widget_wrapper' => ( default => 'Bootstrap' );

	#sub build_form_element_class { ['form-vertical'] }

 __PACKAGE__->meta->make_immutable;

 1;