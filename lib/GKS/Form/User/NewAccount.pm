package GKS::Form::User::NewAccount;
use HTML::FormHandler::Moose;
use HTML::FormHandler::Types ('NotAllDigits');
#extends 'HTML::FormHandler::Model::DBIC';
#extends 'HTML::FormHandler';
extends 'GKS::Form::User::User' , 'GKS::Form::User::Profile';

with 'HTML::FormHandler::Widget::Theme::Bootstrap3';

	has 'schema' => ( is => 'ro', required => 1 );
   #has '+widget_name_space' => ( default => sub { ['GKS::Form::Widget' ] } );
   #has '+field_name_space' => ( default => sub { ['GKS::Form::Field' ] } );
   
   #has '+item_class' => ( default => 'Account' );
   has_field 'account_name' => (label => 'Account Name', required => 1,  minlength => 4, wrapper_class => 'required', 
   								element_attr => {required => 'required', placeholder => 'Account Name'});
   #has_field 'userpasswd' => ( type => 'UserPassword' );
   #has_field 'userprofile' => ( type => 'UserProfile' );
   #has_field 'status' => ( type => 'Select', label => 'Status', empty_select => '-- Choose --', sort_column=>'sort_order', active_column => 'active',);
   #has 'created' => ( isa => 'DateTime', is => 'rw' );
   #has 'account_uuid' => ( isa => 'Str', is => 'rw' );


#   has_field 'form_actions' => ( type => 'Compound',);
#    has_field 'form_actions.save' => ( widget => 'ButtonTag', type => 'Submit',
#        value => 'Save', style => 'width: 100px;', element_class => 'btn btn-success' );
#    has_field 'form_actions.cancel' => ( widget => 'ButtonTag', type => 'Reset',
#        value => 'Cancel', );

 #sub build_form_element_class { ['form-vertical'] }
 
#    before 'update_model' => sub {
#       my $self = shift;
#       $self->item->created( $self->created );
#    };

#sub build_form_tags {
#        {   wrapper_tag => 'div',
#            before => '',
#            after => '</div>',
#        }
#    }


 __PACKAGE__->meta->make_immutable;

 1;