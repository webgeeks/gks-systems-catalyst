package GKS::Form::User::ResetPassword;
use HTML::FormHandler::Moose;

extends 'HTML::FormHandler';
with 'HTML::FormHandler::Widget::Theme::Bootstrap3';


  	has_field 'email_address' => (label => 'Email', required => 1);

	 has_field 'form_actions' => ( type => 'Compound',);
		    has_field 'form_actions.save' => ( widget => 'ButtonTag', type => 'Submit',
	        value => 'Reset Password', element_class => 'btn btn-primary', widget_wrapper => 'None', );


 __PACKAGE__->meta->make_immutable;

 1;