package GKS::Form::User::ChangePassword;
use HTML::FormHandler::Moose;
use HTML::FormHandler::Types ('NotAllDigits');
extends 'HTML::FormHandler';
with 'GKS::Form::Validation';
#with 'GKS::Form::Theme::Bootstrap3';
#with 'GKS::Form::Theme::BootstrapFormMessages';

   #has '+widget_name_space' => ( default => sub { ['GKS::Form::Widget' ] } );
   has_field 'current_password' => (type => 'Password', label => 'Current Password', required => 1);

   has_field 'new_password' => ( type => 'Password', minlength => 8, required => 1,
   								apply => [ NotAllDigits ], validate_method => \&is_strong_password );
   has_field 'confirm_password' => ( type => 'PasswordConf', label => 'Confirm Password',
   									password_field => 'new_password', required => 1,
   									minlength => 8,
   									messages => { pass_conf_not_matched => 'Passwords do not match.' },);

	 has_field 'form_actions' => ( type => 'Compound',);
		    has_field 'form_actions.save' => ( widget => 'ButtonTag', type => 'Submit',
	        value => 'Change Password', style => 'width: 200px;', );


 __PACKAGE__->meta->make_immutable;

 1;