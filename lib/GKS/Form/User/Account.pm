package GKS::Form::User::Account;
use HTML::FormHandler::Moose;
use DateTime;
use DateTime::Format::Strptime;
extends 'HTML::FormHandler::Model::DBIC';
#with 'GKS::Form::Theme::Bootstrap3';

   #has '+widget_name_space' => ( default => sub { ['GKS::Form::Widget' ] } );
   has '+item_class' => ( default => 'Account' );
   has_field 'account_name' => (label => 'Name',);
   #has_field 'status' => ( type => 'Select', label => 'Status',   sort_column=>'sort_order', active_column => 'active',);
   #has_field 'created' => (label => 'Created On', type => 'Date', format => "%m/%d/%Y", element_attr => { readonly => 1, } , element_class => 'noinput fillinblank', noupdate => 1 );
   #has_field 'subscription_date' => (label => 'Subscription Date', type => 'Date', format => "%m/%d/%Y", element_attr => { readonly => 1, } , element_class => 'noinput fillinblank', noupdate => 1 );
   #has_field 'cancelation_date' => (label => 'Cancelation Date', type => 'Date', format => "%m/%d/%Y", element_attr => { readonly => 1, } , element_class => 'noinput fillinblank', noupdate => 1 );

   #has_field 'form_actions' => ( type => 'Compound', wrapper_class => 'plain', noupdate => 1 );
#   has_field 'form_actions.save' => ( widget => 'ButtonTag', type => 'Submit',
#        value => 'Save', element_attr => { style => 'width: 100px;' }, element_class => 'btn btn-success', widget_wrapper => 'None',);
#    has_field 'form_actions.cancel' => ( widget => 'ButtonTag', type => 'Reset',
#        value => 'Cancel', element_class => 'btn btn-outline btn-default', widget_wrapper => 'None',);

   has '+widget_wrapper' => ( default => 'Bootstrap3' );


sub build_form_tags {
        {   wrapper_tag => 'div',
            before => '',
            after => '</div>',
        }
    }


 __PACKAGE__->meta->make_immutable;

 1;