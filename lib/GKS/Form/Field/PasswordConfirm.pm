package GKS::Form::Field::PasswordConfirm;
use HTML::FormHandler::Moose;
#extends 'HTML::FormHandler::Field::Compound';
extends 'HTML::FormHandler';


#   has_field 'new_password' => ( type => 'Password', minlength => 8, required => 1,
#   								validate_method => \&is_strong_password);
#   has_field 'confirm_password' => ( type => 'PasswordConf', label => 'Confirm Password...',
#   									password_field => 'new_password', required => 1,
#   									minlength => 8,
#   									messages => { pass_conf_not_matched => 'Passwords do not match.' },);

   has_field 'new_password' => ( type => 'Password', minlength => 8, required => 1,);
   has_field 'confirm_password' => ( type => 'PasswordConf', label => 'Confirm Password...',
   									password_field => 'new_password', required => 1,
   									minlength => 8,
   									messages => { pass_conf_not_matched => 'Passwords do not match.' },);


sub is_strong_password {
    my $pw = shift;

    return 0 unless $pw =~ /[a-z]/; # Has at least one lowercase
    return 0 unless $pw =~ /[A-Z]/; # Has at least one uppercase
    return 0 unless $pw =~ /\d/;    # Has at least one digit
    return 0 unless length($pw)>=8; # Has at least 8 chars
    #return 0 if $pw =~ / /;         # Has no spaces
    return 1;
}

__PACKAGE__->meta->make_immutable;
1;