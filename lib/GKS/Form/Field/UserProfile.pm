package GKS::Form::Field::UserProfile;
use HTML::FormHandler::Moose;
use Locale::SubCountry;
extends 'HTML::FormHandler::Field::Compound';

   has_field 'type' => (label => 'Profile Type', type => 'Select', options_method => \&options_profiles );
   has_field 'first_name' => (type => 'Text', label => 'First Name', element_attr => {required => 'required', placeholder => 'First' }, required => 1, );
   has_field 'last_name' => (type => 'Text', label => 'Last Name', element_attr => {required => 'required', placeholder => 'Last' }, required => 1, );
   has_field 'email_address' => (label => 'Email', element_attr => {required => 'required', placeholder => 'someone@example.com' }, required => 1, is_html5 => 1, type => 'Email', wrapper_class => 'required');
   has_field 'business_name' => (type => 'Text', label => 'Company', element_attr => {placeholder => 'Company'});
   has_field 'url' => (type => 'Text', label => 'URL', element_attr => {placeholder => 'Website'});
   has_field 'address1' => (type => 'Text', label => 'Address', element_attr => {placeholder => 'Address'} );
   has_field 'address2' => (type => 'Text', label => 'Address Line 2');
   has_field 'city' => (type => 'Text', label => 'City', element_attr => {placeholder => 'City'} );
   has_field 'state' => ( type => 'Select', empty_select => '---Choose---', options_method => \&options_state );
   has_field 'postalcode' => (type => 'Text', label => 'Zip', element_attr => {placeholder => 'Postal Code'} );
   has_field 'phone_mobile' => (type => 'Text', label => 'Phone: Mobile', element_attr => {'data-mask' => '(999) 999-9999'} ); 
   has_field 'phone_work' => (type => 'Text', label => 'Phone: Work', element_attr => {'data-mask' => '(999) 999-9999'} );
   has_field 'phone_fax' => (type => 'Text', label => 'Phone: Fax', element_attr => {'data-mask' => '(999) 999-9999'} );
   has_field 'phone_other' => (type => 'Text', label => 'Phone: Other', element_attr => {'data-mask' => '(999) 999-9999'} );

##NOTE:  FormHander calidates that the input looks like an email address using Email::Valid.
## May want to implement our own with additional checks.
## Should be fine for now...   
   
   sub options_state {
   		##Needs to be dynamic based on Country...
   		my @selections;
	    my $country_code = 'US';
	    my $US = new Locale::SubCountry($country_code);
	    
	    if (  $US->has_sub_countries )
	        {
	            my @state_names  = $US->all_full_names;
	            my @code_names   = $US->all_codes;
	            my %states_keyed_by_code  = $US->code_full_name_hash;
	            my %states_keyed_by_name  = $US->full_name_code_hash;
	
	            foreach my $code ( sort keys %states_keyed_by_code )
	            {
	               #printf("%-3s : %s\n",$code,$states_keyed_by_code{$code});
	                push @selections, { value => $code, label => $code . '-' . $states_keyed_by_code{$code}, note => 'Note' };
	            }
	        }
        return @selections;
     }
     
  sub options_profiles {
  	#my ($self, $c)=@_;
  	 #$c->log->debug('UserID: ' . $c->user->id);
	return (
           10   => 'User',
           20	=> 'Billing',
           30	=> 'Shipping',
           40   => 'Vendor',
           50   => 'Planner',
           
       );
	}
 
 #no HTML::FormHandler::Moose;
 1;
 