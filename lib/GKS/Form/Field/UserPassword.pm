package GKS::Form::Field::UserPassword;
use HTML::FormHandler::Moose;
#use HTML::FormHandler::Types ('NotAllDigits');
extends 'HTML::FormHandler::Field::Compound';

with 'GKS::Form::Validation';

	has_field 'username' => (label => 'Username', required => 1, wrapper_class => 'required',
							build_id_method => \&custom_id, element_attr => {autocomplete => 'off'}, 
							tags => {after_element => '<span id="user-result"></span>'});
	has_field 'role' => ( type => 'Select', options_method => \&options_role );
 	
 	has_field 'new_password' => ( type => 'Password', minlength => 8, required => 1, wrapper_class => 'required',
 								tags => {after_element => '<span class="text-info">Password must contain at least one uppercase, one lowercase and one number and be 8 characters long.</span>'},
   								 element_attr => {autocomplete => 'off'}, validate_method => \&is_strong_password );
   	has_field 'confirm_password' => ( type => 'PasswordConf', label => 'Confirm Password',
   									password_field => 'userpasswd.new_password', required => 1,
   									minlength => 8,
   									messages => { pass_conf_not_matched => 'Passwords do not match.' },);


sub options_role {
	return (
		   2   => 'Admin',
		   3   => 'Management',
		   4   => 'Staff',
           5   => 'User',
           6   => 'Client',
           7   => 'Vendor',
           8   => 'Planner',
           9   => 'Student',
       );
	}

sub custom_id {
        my $self = shift;
        my $full_name = $self->full_name;
        $full_name =~ s/\./_/g;
        return $full_name;
    }

#  apply => [ 'NotAllDigits' ],

 #no HTML::FormHandler::Moose;
 1;
 