package GKS::Form::Theme::Bootstrap3;
use Moose::Role;
#with 'HTML::FormHandler::Widget::Theme::BootstrapFormMessages';

after 'before_build' => sub {
    my $self = shift;
    $self->set_widget_wrapper('Bootstrap3')
       if $self->widget_wrapper eq 'Simple';
};

#sub build_do_form_wrapper {1}
#sub build_form_wrapper_class {['col-lg-8']}

sub build_form_element_class { ['form-horizontal'] }

sub build_update_subfields {{
		all => { element_class => ['form-control'] },
        form_actions => { do_wrapper => 1, do_label => 0, tags => { before_element =>qq{<div class="col-lg-offset-4 col-lg-8">}, after_element =>qq{</div>}} },
        'form_actions.save' => { widget_wrapper => 'None', element_class => ['btn', 'btn-default', 'btn-primary'] },
        'form_actions.cancel' => { widget_wrapper => 'None', element_class => ['btn', 'btn-default'], , element_attr => { style => 'width:80px;' } },
    }}

1;
