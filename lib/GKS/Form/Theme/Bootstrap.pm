package GKS::Form::Theme::Bootstrap;
use Moose::Role;

with 'HTML::FormHandler::Widget::Theme::Bootstrap';

# widget wrapper must be set before the fields are built in BUILD
sub build_do_form_wrapper {1}
sub build_form_wrapper_class {['span8']}
sub build_form_tags {{
    wrapper_tag => 'div',
    after_start => '<fieldset><legend>Controls Bootstrap supports</legend>',
    before_end => '</fieldset>',
}}

#sub build_update_subfields {{
#    input01 => { tags => { after_element => '<p class="help-block">In addition to freeform text, any HTML5 text-based input appears like so.</p>' },
#                 element_class => 'input-xlarge',
#               },
#    optionsCheckbox => { option_label => 'Option one is this and that�be sure to include why it�s great' },
#    fileInput => { element_class => 'input-file' },
#    textarea => { element_class => 'input-xlarge' },
#}}


sub build_update_subfields {{
        focusedInput => { element_class => ['input-xlarge', 'focused'] },
        uneditableInput => { element_class => ['input-xlarge', 'uneditable-input'] },
        disabledInput => { element_class => ['input-xlarge'],
            element_attr => { placeholder => 'Disabled input here�' } },
        optionsCheckbox2 => { element_class => ['checkbox'],
            option_label => 'This is a disabled checkbox' },
        inputError3 => { wrapper_class => ['success'],
            tags => { after_element => qq{\n<span class="help-inline">Woohoo!</span>} } },
        selectError => { wrapper_class => ['success'],
            tags => { after_element => qq{\n<span class="help-inline">Woohoo!</span>} } },
        form_actions => { do_wrapper => 1, do_label => 0 },
        'form_actions.save' => { widget_wrapper => 'None', element_class => ['btn', 'btn-primary'] },
        'form_actions.cancel' => { widget_wrapper => 'None', element_class => ['btn'] },
    }}

1;
