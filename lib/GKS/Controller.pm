package GKS::Controller;

use Moose;
use namespace::autoclean;
use Net::IPAddress;
use Data::Dumper;
BEGIN { extends 'Catalyst::Controller'; }


=head2 AuthUserRole

Verify that the user is logged in and has the proper role(s) for the given action.

=cut

sub AuthUserRole {
	my ( $self, $c, $args ) = @_;

    my $action = $args->{ action } or die 'Attempted authorization check without an action.';
    my $redirect_to_login = $args->{ redirect_to_login } || '/';
	#$c->log->debug('*****Redirect: ' . $redirect);
    # Bounce if user isn't logged in
    unless ( $c->user_exists ) {
    	$c->log->debug('***redirect_after_login: ' . $c->req->path);
        $c->stash( error_msg  => "You must be logged.");
        $c->flash->{redirect_after_login} = $args->{ redirect_after_login } || '/';
        $c->response->redirect( $redirect_to_login );
        return 0;
    }

    # Get role and check it is valid
    my @roles = @{$args->{ role }} or die 'Attempted authorization check without a role.';
    #print Dumper(@roles);
    #$c->log->debug('*****Role(s): ' . $args->{ role });
    unless ( $c->check_any_user_role( @roles ) ) {
    	$c->detach('/error_noperms');
    	return 0;
    }
        
    return 1;
	
}


=head2 LogIP

Convert client IP Address to an INT for storage

=cut

sub logIP {
	my ( $self, $c ) = @_;	
	my $ipnum = ip2num($c->request->address);	
	return $ipnum;	
}


=head1 AUTHOR

The Web Geeks

=head1 COPYRIGHT


=head1 LICENSE


=cut

__PACKAGE__->meta->make_immutable;

1;