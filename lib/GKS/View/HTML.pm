package GKS::View::HTML;

use strict;
use warnings;

use base 'Catalyst::View::TT';

use Template::AutoFilter;
use Template::Stash::XS;

 __PACKAGE__->config(
        # Change default TT extension
        TEMPLATE_EXTENSION => '.tt2',
        render_die => 1,
        CLASS 		=> "Template::AutoFilter",
        STASH => Template::Stash::XS->new,
    );


sub uri_for_css {
  my ($self, $c, $filename) = @_;

  # additional complexity like checking file exists here

  return $c->uri_for('/static/css/' . $filename);
  #return $c->engine->env->{HTTP_HOST}  . '/static/css/' . $filename;
}

sub statuscode_classes {
	my ($self) = @_;
	
	my $statuscodes = { #CSS Classes for Status Codes
        			1 => 'label-primary',
        			2 => 'label-info',
        			3 => 'label-info',
        			4 => 'label-warning',
        			5 => '',
        			6 => 'label-danger',
        	};
    return $statuscodes;
}

1;