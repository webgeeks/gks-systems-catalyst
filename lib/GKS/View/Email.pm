package GKS::View::Email;

use strict;
use base 'Catalyst::View::Email';

=head1 NAME

GKS::View::Email - Email View for GKS

=head1 DESCRIPTION

View for sending email from GKS. 

=head1 AUTHOR

The Web Geeks,,,

=head1 SEE ALSO

L<GKS>

=head1 LICENSE

This library is free software, you can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
