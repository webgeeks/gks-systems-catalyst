use utf8;
package GKS::Schema::Result::User;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::User

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<users>

=cut

__PACKAGE__->table("users");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 accounts_idaccounts

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_uuid

  data_type: 'varchar'
  is_nullable: 1
  size: 36

=head2 username

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 password

  data_type: 'char'
  is_nullable: 1
  size: 72

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 created

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 created_by

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 updated

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 updated_by

  data_type: 'integer'
  is_nullable: 1

=head2 forgot_passwd_token

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 forgot_passwd_token_expires

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 is_primary

  data_type: 'tinyint'
  default_value: 0
  is_nullable: 1

=head2 id_ym

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 id_google

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 id_facebook

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "accounts_idaccounts",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_uuid",
  { data_type => "varchar", is_nullable => 1, size => 36 },
  "username",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "password",
  { data_type => "char", is_nullable => 1, size => 72 },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "created",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "created_by",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "updated",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "updated_by",
  { data_type => "integer", is_nullable => 1 },
  "forgot_passwd_token",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "forgot_passwd_token_expires",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "is_primary",
  { data_type => "tinyint", default_value => 0, is_nullable => 1 },
  "id_ym",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "id_google",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "id_facebook",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=item * L</accounts_idaccounts>

=back

=cut

__PACKAGE__->set_primary_key("id", "accounts_idaccounts");

=head1 UNIQUE CONSTRAINTS

=head2 C<uid_UNIQUE>

=over 4

=item * L</user_uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("uid_UNIQUE", ["user_uuid"]);

=head1 RELATIONS

=head2 accounts_idaccount

Type: belongs_to

Related object: L<GKS::Schema::Result::Account>

=cut

__PACKAGE__->belongs_to(
  "accounts_idaccount",
  "GKS::Schema::Result::Account",
  { idaccounts => "accounts_idaccounts" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);

=head2 user_logins

Type: has_many

Related object: L<GKS::Schema::Result::UserLogin>

=cut

__PACKAGE__->has_many(
  "user_logins",
  "GKS::Schema::Result::UserLogin",
  {
    "foreign.users_accounts_idaccounts" => "self.accounts_idaccounts",
    "foreign.users_id" => "self.id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 user_profiles

Type: has_many

Related object: L<GKS::Schema::Result::UserProfile>

=cut

__PACKAGE__->has_many(
  "user_profiles",
  "GKS::Schema::Result::UserProfile",
  {
    "foreign.users_accounts_idaccounts" => "self.accounts_idaccounts",
    "foreign.users_id" => "self.id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 user_roles

Type: has_many

Related object: L<GKS::Schema::Result::UserRole>

=cut

__PACKAGE__->has_many(
  "user_roles",
  "GKS::Schema::Result::UserRole",
  { "foreign.user_id" => "self.id" },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 roles

Type: many_to_many

Composing rels: L</user_roles> -> role

=cut

__PACKAGE__->many_to_many("roles", "user_roles", "role");


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-24 22:58:15
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ZXpaKUJTTZLaQBXHV+VnHg

# You can replace this text with custom code or comments, and it will be preserved on regeneration

# many_to_many():
#   args:
#     1) Name of relationship, DBIC will create accessor with this name
#     2) Name of has_many() relationship this many_to_many() is shortcut for
#     3) Name of belongs_to() relationship in model class of has_many() above
#   You must already have the has_many() defined to use a many_to_many().


# Have the 'password' column use a SHA-1 hash and 20-byte salt
# with RFC 2307 encoding; Generate the 'check_password" method
__PACKAGE__->add_columns(
    '+password' => {
        passphrase       => 'rfc2307',
        passphrase_class => 'BlowfishCrypt',
        passphrase_args  => {
            cost        => 14,
            salt_random => 20,
        },
        passphrase_check_method => 'check_password',
    },
);

__PACKAGE__->add_columns(
    "created",
    { data_type => 'timestamp', set_on_create => 1 },
    "updated",
    { data_type => 'timestamp', set_on_create => 1, set_on_update => 1 },
);

__PACKAGE__->belongs_to(
    'status',
    'GKS::Schema::Result::Status',
    { id => 'status' },
    { join_type => 'left' }
);

__PACKAGE__->has_one(
	'accounts',
	'GKS::Schema::Result::Account',
	{ "foreign.idaccounts" => "self.accounts_idaccounts" }
);


=head2 has_role

Check if a user has the specified role

=cut

use Perl6::Junction qw/any/;
sub has_role {
    my ($self, $role) = @_;

    # Does this user posses the required role?
    return any(map { $_->role } $self->roles) eq $role;
}


#use Email::Valid;
#sub new {
#  my ($class, $args) = @_;
#
#  if( exists $args->{email_address}
#      && !Email::Valid->address($args->{email_address}) ) {
#      die 'Email invalid';
#  }
#
#  return $class->next::method($args);
#}



__PACKAGE__->meta->make_immutable;
1;
