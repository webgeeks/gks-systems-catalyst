use utf8;
package GKS::Schema::Result::Account;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::Account

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<accounts>

=cut

__PACKAGE__->table("accounts");

=head1 ACCESSORS

=head2 idaccounts

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 account_uuid

  data_type: 'varchar'
  is_nullable: 1
  size: 36

=head2 account_name

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 account_type

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 created_by

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 created

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 subscription_date

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 cancelation_date

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 import_src

  data_type: 'varchar'
  is_nullable: 1
  size: 16

=head2 import_id

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 last_modified

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "idaccounts",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "account_uuid",
  { data_type => "varchar", is_nullable => 1, size => 36 },
  "account_name",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "account_type",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "created_by",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "created",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "subscription_date",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "cancelation_date",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "import_src",
  { data_type => "varchar", is_nullable => 1, size => 16 },
  "import_id",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "last_modified",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</idaccounts>

=back

=cut

__PACKAGE__->set_primary_key("idaccounts");

=head1 UNIQUE CONSTRAINTS

=head2 C<account_uuid_UNIQUE>

=over 4

=item * L</account_uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("account_uuid_UNIQUE", ["account_uuid"]);

=head1 RELATIONS

=head2 users

Type: has_many

Related object: L<GKS::Schema::Result::User>

=cut

__PACKAGE__->has_many(
  "users",
  "GKS::Schema::Result::User",
  { "foreign.accounts_idaccounts" => "self.idaccounts" },
  { cascade_copy => 0, cascade_delete => 0 },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-30 01:39:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:CwpUuqWLBjpp+tUKpgLC2w
##########################################################################

__PACKAGE__->add_columns(
    "created",
    { data_type => 'timestamp', set_on_create => 1 },
    "last_modified",
    { data_type => 'timestamp', set_on_create => 1, set_on_update => 1 },
);

__PACKAGE__->belongs_to(
    'status',
    'GKS::Schema::Result::Status',
    { id => 'status' },
);


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
