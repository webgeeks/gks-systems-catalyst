use utf8;
package GKS::Schema::Result::UserProfile;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::UserProfile

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<user_profiles>

=cut

__PACKAGE__->table("user_profiles");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 users_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 users_accounts_idaccounts

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 type

  data_type: 'integer'
  is_nullable: 1

=head2 first_name

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 last_name

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 email_address

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 title

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 dob

  data_type: 'date'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 business_name

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 address1

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 address2

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 city

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 state

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 province

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 postalcode

  data_type: 'varchar'
  is_nullable: 1
  size: 18

=head2 country_code

  data_type: 'char'
  is_nullable: 1
  size: 2

=head2 phone_home

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 phone_mobile

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 phone_work

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 phone_other

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 phone_fax

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 url

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 status

  data_type: 'integer'
  is_nullable: 1

=head2 is_primary

  data_type: 'tinyint'
  is_nullable: 1

=head2 gender

  data_type: 'enum'
  extra: {list => ["M","F"]}
  is_nullable: 1

=head2 email_verified

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 language

  data_type: 'varchar'
  is_nullable: 1
  size: 12

=head2 ethnicity

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 bad_email

  data_type: 'tinyint'
  is_nullable: 1

=head2 bad_address

  data_type: 'tinyint'
  is_nullable: 1

=head2 welcome_email

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 created

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 created_by

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 deleted

  data_type: 'datetime'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 deleted_by

  data_type: 'integer'
  is_nullable: 1

=head2 last_modified

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 last_modified_by

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "users_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "users_accounts_idaccounts",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "type",
  { data_type => "integer", is_nullable => 1 },
  "first_name",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "last_name",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "email_address",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "title",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "dob",
  { data_type => "date", datetime_undef_if_invalid => 1, is_nullable => 1 },
  "business_name",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "address1",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "address2",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "city",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "state",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "province",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "postalcode",
  { data_type => "varchar", is_nullable => 1, size => 18 },
  "country_code",
  { data_type => "char", is_nullable => 1, size => 2 },
  "phone_home",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "phone_mobile",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "phone_work",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "phone_other",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "phone_fax",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "url",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "status",
  { data_type => "integer", is_nullable => 1 },
  "is_primary",
  { data_type => "tinyint", is_nullable => 1 },
  "gender",
  { data_type => "enum", extra => { list => ["M", "F"] }, is_nullable => 1 },
  "email_verified",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "language",
  { data_type => "varchar", is_nullable => 1, size => 12 },
  "ethnicity",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "bad_email",
  { data_type => "tinyint", is_nullable => 1 },
  "bad_address",
  { data_type => "tinyint", is_nullable => 1 },
  "welcome_email",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "created",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "created_by",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "deleted",
  {
    data_type => "datetime",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "deleted_by",
  { data_type => "integer", is_nullable => 1 },
  "last_modified",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "last_modified_by",
  { data_type => "varchar", is_nullable => 1, size => 128 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=item * L</users_id>

=item * L</users_accounts_idaccounts>

=back

=cut

__PACKAGE__->set_primary_key("id", "users_id", "users_accounts_idaccounts");

=head1 RELATIONS

=head2 user

Type: belongs_to

Related object: L<GKS::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "GKS::Schema::Result::User",
  {
    accounts_idaccounts => "users_accounts_idaccounts",
    id => "users_id",
  },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 user_categories

Type: has_many

Related object: L<GKS::Schema::Result::UserCategory>

=cut

__PACKAGE__->has_many(
  "user_categories",
  "GKS::Schema::Result::UserCategory",
  {
    "foreign.user_profiles_id"                        => "self.id",
    "foreign.user_profiles_users_accounts_idaccounts" => "self.users_accounts_idaccounts",
    "foreign.user_profiles_users_id"                  => "self.users_id",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 service_categories_idservice_categories

Type: many_to_many

Composing rels: L</user_categories> -> service_categories_idservice_category

=cut

__PACKAGE__->many_to_many(
  "service_categories_idservice_categories",
  "user_categories",
  "service_categories_idservice_category",
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-30 01:39:38
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:2uTnSA/4V/OrMO6vF7z95g

__PACKAGE__->add_columns(
    "created",
    { data_type => 'datetime', set_on_create => 1 },
    "last_modified",
    { data_type => 'timestamp', set_on_create => 1, set_on_update => 1 },
);

__PACKAGE__->belongs_to(
    'status',
    'GKS::Schema::Result::Status',
    { id => 'status' },
);


# FormHandler checks e-mail address for us.
# We shouldn't need this...
#use Email::Valid;
#sub new {
#  my ($class, $args) = @_;
#
#  if( exists $args->{email_address}
#      && !Email::Valid->address($args->{email_address}) ) {
#      die 'Email invalid';
#  }
#
#  return $class->next::method($args);
#}


#sub get_profiles {
#    my ($self, $idaccounts) = @_;
#
#    return $self->search({
#        accounts_idaccounts => $idaccounts,
#        'user_profiles.type' => 10,
#    },
#    {
#    	join => ['user_profiles'],
#    	columns => [qw(user_uuid username)],
#    	#collapse => 1,
#    	'+columns' => [qw(user_profiles.id user_profiles.first_name user_profiles.last_name user_profiles.email_address)],
#    	order_by => ['username'],
#    }
#    );
#}


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
