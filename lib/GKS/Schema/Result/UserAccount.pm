use utf8;
package GKS::Schema::Result::UserAccount;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::UserAccount - VIEW

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<UserAccounts>

=cut

__PACKAGE__->table("UserAccounts");

=head1 ACCESSORS

=head2 account_uuid

  data_type: 'varchar'
  is_nullable: 1
  size: 36

=head2 account_name

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 username

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 email_address

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 first_name

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 last_name

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "account_uuid",
  { data_type => "varchar", is_nullable => 1, size => 36 },
  "account_name",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "username",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "email_address",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "first_name",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "last_name",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-03-31 16:05:30
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:o/nP5EcsES6vxKE5r3jjKg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
