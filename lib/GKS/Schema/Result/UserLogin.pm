use utf8;
package GKS::Schema::Result::UserLogin;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::UserLogin

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<user_logins>

=cut

__PACKAGE__->table("user_logins");

=head1 ACCESSORS

=head2 iduser_logins

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 users_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 users_accounts_idaccounts

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 last_log_in

  data_type: 'timestamp'
  datetime_undef_if_invalid: 1
  is_nullable: 1

=head2 log_in_ip

  data_type: 'integer'
  extra: {unsigned => 1}
  is_nullable: 1

=head2 log_in_success

  data_type: 'tinyint'
  is_nullable: 1

=head2 user_agent

  data_type: 'varchar'
  is_nullable: 1
  size: 128

=head2 authen_src

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=cut

__PACKAGE__->add_columns(
  "iduser_logins",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "users_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "users_accounts_idaccounts",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "last_log_in",
  {
    data_type => "timestamp",
    datetime_undef_if_invalid => 1,
    is_nullable => 1,
  },
  "log_in_ip",
  { data_type => "integer", extra => { unsigned => 1 }, is_nullable => 1 },
  "log_in_success",
  { data_type => "tinyint", is_nullable => 1 },
  "user_agent",
  { data_type => "varchar", is_nullable => 1, size => 128 },
  "authen_src",
  { data_type => "varchar", is_nullable => 1, size => 45 },
);

=head1 PRIMARY KEY

=over 4

=item * L</iduser_logins>

=item * L</users_id>

=item * L</users_accounts_idaccounts>

=back

=cut

__PACKAGE__->set_primary_key("iduser_logins", "users_id", "users_accounts_idaccounts");

=head1 RELATIONS

=head2 user

Type: belongs_to

Related object: L<GKS::Schema::Result::User>

=cut

__PACKAGE__->belongs_to(
  "user",
  "GKS::Schema::Result::User",
  {
    accounts_idaccounts => "users_accounts_idaccounts",
    id => "users_id",
  },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-27 19:03:16
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:WFaT80YtWvn7Ajq5r7wcxg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
