use utf8;
package GKS::Schema::Result::UserCategory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::UserCategory

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<user_categories>

=cut

__PACKAGE__->table("user_categories");

=head1 ACCESSORS

=head2 user_profiles_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_profiles_users_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 user_profiles_users_accounts_idaccounts

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 service_categories_idservice_categories

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "user_profiles_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_profiles_users_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "user_profiles_users_accounts_idaccounts",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "service_categories_idservice_categories",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</user_profiles_id>

=item * L</user_profiles_users_id>

=item * L</user_profiles_users_accounts_idaccounts>

=item * L</service_categories_idservice_categories>

=back

=cut

__PACKAGE__->set_primary_key(
  "user_profiles_id",
  "user_profiles_users_id",
  "user_profiles_users_accounts_idaccounts",
  "service_categories_idservice_categories",
);

=head1 RELATIONS

=head2 service_categories_idservice_category

Type: belongs_to

Related object: L<GKS::Schema::Result::ServiceCategory>

=cut

__PACKAGE__->belongs_to(
  "service_categories_idservice_category",
  "GKS::Schema::Result::ServiceCategory",
  {
    idservice_categories => "service_categories_idservice_categories",
  },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);

=head2 user_profile

Type: belongs_to

Related object: L<GKS::Schema::Result::UserProfile>

=cut

__PACKAGE__->belongs_to(
  "user_profile",
  "GKS::Schema::Result::UserProfile",
  {
    id => "user_profiles_id",
    users_accounts_idaccounts => "user_profiles_users_accounts_idaccounts",
    users_id => "user_profiles_users_id",
  },
  { is_deferrable => 1, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-03-31 16:05:30
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:UKinQwCqs629mBO/EfdrUQ


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
