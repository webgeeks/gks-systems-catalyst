use utf8;
package GKS::Schema::Result::Address;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::Address

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<addresses>

=cut

__PACKAGE__->table("addresses");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 type

  data_type: 'integer'
  is_nullable: 1

=head2 profileid

  data_type: 'integer'
  is_nullable: 1

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 address1

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 address2

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 city

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 st

  data_type: 'varchar'
  is_nullable: 1
  size: 4

=head2 province

  data_type: 'varchar'
  is_nullable: 1
  size: 64

=head2 postalcode

  data_type: 'varchar'
  is_nullable: 1
  size: 18

=head2 country_code3

  data_type: 'char'
  default_value: 'USA'
  is_nullable: 1
  size: 3

=head2 profiles_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=head2 profiles_users_id

  data_type: 'integer'
  is_foreign_key: 1
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "type",
  { data_type => "integer", is_nullable => 1 },
  "profileid",
  { data_type => "integer", is_nullable => 1 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "address1",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "address2",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "city",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "st",
  { data_type => "varchar", is_nullable => 1, size => 4 },
  "province",
  { data_type => "varchar", is_nullable => 1, size => 64 },
  "postalcode",
  { data_type => "varchar", is_nullable => 1, size => 18 },
  "country_code3",
  { data_type => "char", default_value => "USA", is_nullable => 1, size => 3 },
  "profiles_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "profiles_users_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=item * L</profiles_id>

=item * L</profiles_users_id>

=back

=cut

__PACKAGE__->set_primary_key("id", "profiles_id", "profiles_users_id");

=head1 RELATIONS

=head2 profile

Type: belongs_to

Related object: L<GKS::Schema::Result::Profile>

=cut

__PACKAGE__->belongs_to(
  "profile",
  "GKS::Schema::Result::Profile",
  { id => "profiles_id", users_id => "profiles_users_id" },
  { is_deferrable => 1, on_delete => "CASCADE", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07036 @ 2014-06-19 16:24:06
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:6ElO6UiRnbPOqp4pZ86s6Q


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
