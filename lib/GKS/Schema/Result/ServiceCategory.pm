use utf8;
package GKS::Schema::Result::ServiceCategory;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

GKS::Schema::Result::ServiceCategory

=cut

use strict;
use warnings;

use Moose;
use MooseX::NonMoose;
use MooseX::MarkAsMethods autoclean => 1;
extends 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=item * L<DBIx::Class::TimeStamp>

=item * L<DBIx::Class::PassphraseColumn>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp", "PassphraseColumn");

=head1 TABLE: C<service_categories>

=cut

__PACKAGE__->table("service_categories");

=head1 ACCESSORS

=head2 idservice_categories

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 service_id

  data_type: 'integer'
  is_nullable: 0

=head2 a8

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 category

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 sub_category

  data_type: 'varchar'
  is_nullable: 1
  size: 45

=head2 sort_order

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "idservice_categories",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "service_id",
  { data_type => "integer", is_nullable => 0 },
  "a8",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "category",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "sub_category",
  { data_type => "varchar", is_nullable => 1, size => 45 },
  "sort_order",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</idservice_categories>

=back

=cut

__PACKAGE__->set_primary_key("idservice_categories");

=head1 RELATIONS

=head2 user_categories

Type: has_many

Related object: L<GKS::Schema::Result::UserCategory>

=cut

__PACKAGE__->has_many(
  "user_categories",
  "GKS::Schema::Result::UserCategory",
  {
    "foreign.service_categories_idservice_categories" => "self.idservice_categories",
  },
  { cascade_copy => 0, cascade_delete => 0 },
);

=head2 user_profiles

Type: many_to_many

Composing rels: L</user_categories> -> user_profile

=cut

__PACKAGE__->many_to_many("user_profiles", "user_categories", "user_profile");


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-03-31 16:05:30
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:EfK3NUKRZ/TBhW+3KkQCjg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
__PACKAGE__->meta->make_immutable;
1;
