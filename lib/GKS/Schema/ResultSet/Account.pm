package GKS::Schema::ResultSet::Account;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

=head2 get_account_by_uuid

A predefined search for to get Accounts by UUID

=cut

sub get_account_by_uuid {
    my ($self, $account_uuid) = @_;

    return $self->find({
        account_uuid => $account_uuid
    });
}

sub get_user_account_by_uuid {
    my ($self, $user_uuid) = @_;

    return $self->find({
        'users.user_uuid' => $user_uuid
    },
    {
    	join => ["users"],
    	columns => [qw(account_uuid)],
    	'+columns' => [qw(users.user_uuid users.id)],
    	#prefetch => 'users' # return artist data too!
    });
}


=head2 created_after

A predefined search for recently added books

=cut

sub created_after {
    my ($self, $datetime) = @_;

    my $date_str = $self->result_source->schema->storage
                          ->datetime_parser->format_datetime($datetime);

    return $self->search({
        created => { '>' => $date_str }
    });
}


=head2 title_like

A predefined search for books with a 'LIKE' search in the string

=cut

sub title_like {
    my ($self, $title_str) = @_;

    return $self->search({
        title => { 'like' => "%$title_str%" }
    });
}


1;