package GKS::Schema::ResultSet::UserRole;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';


sub user_role_id {
    my ($self, $userid) = @_;

	my $roleid = $self->search(
    	{user_id => $userid},
    )->get_column('role_id');

	return $roleid->min;
}


1;