package GKS::Schema::ResultSet::User;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

#ResultSet: predefined queries

sub created_after {
    my ($self, $datetime) = @_;

    my $date_str = $self->result_source->schema->storage
                          ->datetime_parser->format_datetime($datetime);

    return $self->search({
        created => { '>' => $date_str }
    });
}

#A predefined search for to get Accounts by UUID
sub users_list {
    my ($self, $idaccounts) = @_;

    return $self->search({
        #'accounts.account_uuid' => $idaccounts,
        accounts_idaccounts => $idaccounts,
        'user_profiles.deleted' => { '=', undef }, # IS NULL
        'user_profiles.type' => { '-not_in' => '20, 30'}, # No Billing or Shipping Profiles
    },
    {
    	join => [qw(user_profiles)],
    	#join => ['accounts'],
    	#join => {'user_profiles' => 'accounts'},
    	columns => [qw(user_uuid username is_primary status user_profiles.last_name)],
    	#collapse => 1,
    	'+columns' => [qw(user_profiles.id user_profiles.first_name user_profiles.email_address user_profiles.business_name)],
    	order_by => ['user_profiles.last_name'],
    }
    );
}


sub user_exists {
    my ($self, $username) = @_;

    return $self->count({
        username => $username
    });
}


#$schema->resultset('CD')->search(
#  { 'Title' => 'Funky CD',
#  },
#  { prefetch      => 'tracks',
#    order_by  => ['tracks.id'],
#  }
#);

1;