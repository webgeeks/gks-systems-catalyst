package GKS::Schema::ResultSet::Profile;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';


sub all {
    my ($self) = @_;

    return $self->search();
}


sub get_user_profiles {
    my ($self, $user_uuid) = @_;

    return $self->search({
        user_uuid => $user_uuid,
        is_primary => 1,
    },
    {
    	join => ["users"],
    	#join => ['accounts'],
    	#join => {'user_profiles' => 'accounts'},
    	#columns => [qw(user_uuid username is_primary status)],
    	#collapse => 1,
    	#'+columns' => [qw(user_profiles.id user_profiles.first_name user_profiles.last_name user_profiles.email_address user_profiles.business_name)],
    	#order_by => ['username'],
    }
    );
}

1;