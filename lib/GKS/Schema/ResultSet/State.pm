package GKS::Schema::ResultSet::State;

use strict;
use warnings;
use base 'DBIx::Class::ResultSet';


sub state_list {
    my ($self) = @_;

    return $self->search();
}


1;