package GKS::Controller::Students::Dashboard;
use Moose;
use namespace::autoclean;

BEGIN { extends 'GKS::Controller'; }

=head1 NAME

GKS::Controller::Admin::Dashboard - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base : Chained('/base') PathPart('students/dashboard') CaptureArgs(0){
    my ($self, $c) = @_;

    # Print a message to the debug log
    #$c->log->debug('*** INSIDE BASE METHOD: Dashboard ***');
    
    # Stash the controller name
	$c->stash->{ controller } = 'Admin::Dashboard';
	
	# Set the Wrapper for the Controller
	$c->stash->{ wrapper } = $c->config->{wrapper}->{'students'};

    # Load status messages
    $c->load_status_msgs;
}


sub index :Chained('base') :PathPart('') :Args(0) {
    my ($self, $c) = @_;
    
#	# Check to make sure user has the Access to Dashboard
#	my @roles = qw(superuser admin mgmt staff);
#	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
#	my $redirect_after_login = $c->req->uri;
#	return 0 unless $self->AuthUserRole($c, {
#		action => 'Admin Dashboard',
#		role   => \@roles, 
#		redirect => $redirect_to_login,
#		redirect_after_login => $redirect_after_login
#	});

 $c->response->body('Matched GKS::Controller::Students::Dashboard in Students.');

}

__PACKAGE__->meta->make_immutable;

1;
