package GKS::Controller::Admin::User;
use Moose;
use namespace::autoclean;
use Data::UUID;
use DateTime;
use DateTime::Format::Strptime;
use Data::Dumper;
use Captcha::reCAPTCHA;
# Load our Forms
#use GKS::Form::User::Account;
use GKS::Form::User::ChangePassword;
use GKS::Form::User::Signin;
use GKS::Form::User::NewAccount;

#use GKS::Form::User::Signup;

BEGIN { extends 'GKS::Controller'; }

=head1 NAME

GKS::Controller::Admin::User - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller for Administrator User/Account functions.

=head1 METHODS

=cut

=head2 base

Set up the path.

=cut

sub base : Chained('/base') : PathPart('admin/user') : CaptureArgs(0) {
	my ( $self, $c ) = @_;

	# Stash the upload_dir setting
	#$c->stash->{ upload_dir } = $c->config->{ upload_dir };

	# Stash the controller name
	$c->stash->{controller} = 'Admin::User';

	# Set the Wrapper for the Controller
	$c->stash->{wrapper} = $c->config->{wrapper}->{'admin'};

	# Load status messages
	$c->load_status_msgs;
}

#=head2 index
#
#Bounce to list of users.
#
#=cut
#
#sub index : Chained( 'base' ) : PathPart( '' ) : Args( 0 ) {
#	my ( $self, $c ) = @_;
#
#	#$c->go( 'dashboard' );
#	$c->response->body('Matched GKS::Controller::User in User.');
#}

=head2 accounts

Accounts Landing Page

=cut

sub account : Chained('base') : PathPart('accounts') : Args(0) {
	my ( $self, $c ) = @_;

	# Check to make sure user has the Access to Accounts
	my @roles = qw(superuser admin mgmt staff);
	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
	my $redirect_after_login = $c->req->uri;
	return 0 unless $self->AuthUserRole($c, {
		action => 'Accounts',
		role   => \@roles, 
		redirect => $redirect_to_login,
		redirect_after_login => $redirect_after_login
	});
	
	my $userid = $c->user->id;
	my $roles_rs = $c->model('DB::UserRole')->search({user_id => $userid},{ order_by => 'role_id', rows => 1 })->single;
	my $roleid = $roles_rs->get_column('role_id');
	##$c->stash(roleidfoobar => [$c->model('DB::UserRole')->search({user_id => $userid},{ order_by => 'role_id', rows => 1 })->single]);
	$c->log->debug('RoleID: ' . $roleid);

}

=head2 account_new

Add new Accounts to the System

=cut

sub accountnew : Chained('base') PathPart('account_new') Args(0) {
	my ( $self, $c ) = @_;

	# Check to make sure user has the right to Add Accounts
	my @roles = qw(superuser admin mgmt staff);
	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
	my $redirect_after_login = $c->req->uri;
	return 0 unless $self->AuthUserRole($c, {
		action => 'Add Account',
		role   => \@roles, 
		redirect => $redirect_to_login,
		redirect_after_login => $redirect_after_login
	});

	my $schema = $c->model('DB')->schema;
	my $user   = $c->model('DB::User');

	#$ENV{REMOTE_ADDR} = $c->req->address;
	my $form = GKS::Form::User::NewAccount->new;

	# Data::UUID seems to generate better (less predictable) UUID's than MySQL
	my $ug        = new Data::UUID;
	my $uuid      = $ug->create_str();
	my $user_uuid = $ug->create_str();
	
	$uuid = $c->model("UUID")->gen_UUID;

	#$c->log->debug('User: ' . $c->user->username);
	#$c->log->debug('*****************************************\n*************************');
	$form->process(
		posted => ( $c->req->method eq 'POST' ),
		params => $c->request->body_parameters
	);

	if ( $form->ran_validation ) {

		# Verify username doesn't already exist
		my $username = $c->request->params->{'userpasswd.username'};
		#my $count = $c->model('DB::User')->user_exists( $username );
		if ( $c->model('DB::User')->user_exists( $username ) > 0 ) {

	 #$form->add_form_error("<div> - User already exists in the system.</div>");
			$form->field('userpasswd.username')
			  ->add_error("<div> - User already exists in the system.</div>");
		}
	}

	if ( $form->validated ) {

		# Create Account, User and related records
		my $ug        = new Data::UUID;
		my $uuid      = $ug->create_str();
		my $user_uuid = $ug->create_str();
		my $username  = $c->request->params->{'userpasswd.username'};
		my $password  = $c->request->params->{'userpasswd.new_password'};

		# Create the Account
		my $account = $c->model('DB::Account')->create(
			{
				account_uuid => $uuid,
				status       => '1',
				account_name => $c->request->params->{account_name},
			}
		);

		#print Dumper($account->get_column('idaccounts'));
		my $account_id = $account->get_column('idaccounts');

		# Create New User
		my $user = $c->model('DB::User')->create(
			{
				user_uuid           => $user_uuid,
				accounts_idaccounts => $account_id,
				created_by          => $c->user->username,
				status              => '1',
				is_primary          => '1',
				username            => $username,
				password            => $password,
			}
		);

		# Insert Role
		$user->create_related( 'user_roles',
			{ role_id => $c->request->params->{'userpasswd.role'} } );

		# Create Profile
		$user->create_related(
			'user_profiles',
			{
				type       => $c->request->params->{'userprofile.type'},
				status     => '1',
				first_name => $c->request->params->{'userprofile.first_name'},
				last_name  => $c->request->params->{'userprofile.last_name'},
				email_address =>
				  $c->request->params->{'userprofile.email_address'},
				business_name =>
				  $c->request->params->{'userprofile.business_name'},
				address1     => $c->request->params->{'userprofile.address1'},
				address2     => $c->request->params->{'userprofile.address2'},
				city         => $c->request->params->{'userprofile.city'},
				state        => $c->request->params->{'userprofile.state'},
				postalcode   => $c->request->params->{'userprofile.postalcode'},
				country_code => 'US',
				url          => $c->request->params->{'userprofile.url'},
				phone_mobile =>
				  $c->request->params->{'userprofile.phone_mobile'},
				phone_work  => $c->request->params->{'userprofile.phone_work'},
				phone_fax   => $c->request->params->{'userprofile.phone_fax'},
				phone_other => $c->request->params->{'userprofile.phone_other'},
				created_by  => $c->user->username,
				is_primary  => '1',

			}
		);

		# Now Display Account
		$c->response->redirect( 'Admin::User', 'account', $account_id,
			'display' );
		return;
	}
	else {
		$c->stash( form => $form, );
	}

}

=head2 accounts

Display Specified Account

=cut

sub account_edit : Chained('get_account') : PathPart('display') : Args(0) {
	my ( $self, $c ) = @_;

	# Check to make sure user has the Access to Accounts
	my @roles = qw(superuser admin mgmt staff);
	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
	my $redirect_after_login = $c->req->uri;
	return 0 unless $self->AuthUserRole($c, {
		action => 'Display Account',
		role   => \@roles, 
		redirect => $redirect_to_login,
		redirect_after_login => $redirect_after_login
	});
	
	my $schema = $c->model('DB')->schema;
	my $account = $c->stash->{account};
	my $form = GKS::Form::User::Account->new(schema => $schema );
	$form->process(posted => ($c->req->method eq 'POST'),
							item => $account,
							params => $c->request->body_parameters);
	#Get the Users for this Account
	#my $users = $c->model('DB::User')->users_list($account->idaccount);	
	
    $c->stash(
    		form => $form,
    		account_uuid => $account->account_uuid,
    		users => [$c->model('DB::User')->users_list($account->idaccounts)]
    	);
	

}

=head2 get_account

Get the Account Record

=cut

sub get_account : Chained('base') : PathPart('account') : CaptureArgs(1) {
	my ( $self, $c, $account_id ) = @_;

	my $account =
	  $c->model('DB::Account')->get_account_by_uuid( $account_id );
	$c->stash->{account} = $account;
	#$c->stash(object => $account);
}

=head2 get_account

Get the Account Record

=cut

sub goto_account : Chained('base') : PathPart('gotoaccount') : Args(0) {
	my ( $self, $c ) = @_;

	my $account_id = '97460451-49b9-4b81-8c6c-8d6cb506ae56';

#$c->response->redirect( $c->uri_for('/admin/user/account/' . $account_id . '/display') );
	my @params = ($account_id);
	$c->response->redirect(
		$c->uri_for(
			$c->controller('Admin::User')->action_for('display'), @params
		)
	);
	$c->detach;

	#return;
}

=head2 index

Login logic

=cut


=head2 accounts

Valid Username Check

=cut

#sub validuser : Chained('base') : PathPart('validuser') : Args(0) {
#	my ( $self, $c ) = @_;
#	$c->stash->{no_wrapper} = 1;
#	
##	# Check to make sure user has the right to Validate User
##	my @roles = qw(superuser admin mgmt staff);
##	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
##	my $redirect_after_login = $c->req->uri;
##	return 0 unless $self->AuthUserRole($c, {
##		action => 'Validate User',
##		role   => \@roles, 
##		redirect => $redirect_to_login,
##		redirect_after_login => $redirect_after_login
##	});
#	
#	if ($c->request->referer =~ /lovegevity/){
#	
#	# Verify username doesn't already exist
#	my $username = $c->request->params->{'userpasswd.username'};
#	my $count = $c->model('DB::User')->user_exists($username);
#	if ( $c->model('DB::User')->user_exists($username) > 0 ) {
#
#		#print "Yes!";
#		#$c->log->debug($form->has_form_errors);
#		$c->log->debug( "Username: " . $count . ':' . $username );
#
#		$c->response->body(
#'<i class="fa fa-exclamation text-danger"></i><span class="text-danger"> User already exists in the system.</span>'
#		);
#	}
#	else {
#		$c->response->body(
#'<i class="fa fa-check text-success"></i><span class="text-success"> Username Available</span>'
#		);
#	}
#	}
#
#}

=encoding utf8

=head1 AUTHOR

The Web Geeks,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
