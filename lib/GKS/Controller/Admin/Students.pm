package GKS::Controller::Admin::Students;
use Moose;
use namespace::autoclean;

BEGIN { extends 'GKS::Controller'; }

sub base : Chained('/base') PathPart('admin/students') CaptureArgs(0){
    my ($self, $c) = @_;
    if ( !$c->user_exists() ) {
		$c->go( 'User', 'signin' );
	}

    # Print a message to the debug log
    #$c->log->debug('*** INSIDE BASE METHOD: Dashboard ***');
    
    # Stash the controller name
	$c->stash->{ controller } = 'Admin::Students';
	
	# Set the Wrapper for the Controller
	$c->stash->{ wrapper } = $c->config->{wrapper}->{'admin'};

    # Load status messages
    $c->load_status_msgs;
}


sub index :Chained('base') :PathPart('') :Args(0) {
    my ($self, $c) = @_;
    
	# Check to make sure user has the Access to Dashboard
	my @roles = qw(superuser admin mgmt staff);
	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
	my $redirect_after_login = $c->req->uri;
	return 0 unless $self->AuthUserRole($c, {
		action => 'Student Admin',
		role   => \@roles, 
		redirect => $redirect_to_login,
		redirect_after_login => $redirect_after_login
	});

}


__PACKAGE__->meta->make_immutable;

1;
