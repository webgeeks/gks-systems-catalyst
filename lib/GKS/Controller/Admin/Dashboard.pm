package GKS::Controller::Admin::Dashboard;
use Moose;
use namespace::autoclean;
use Data::Dumper;


BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

GKS::Controller::Admin::Dashboard - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base : Chained('/') PathPart('admin') CaptureArgs(0){
    my ($self, $c) = @_;
    if ( !$c->user_exists() ) {
		$c->go( 'User', 'signin' );
	}

    # Print a message to the debug log
    #$c->log->debug('*** INSIDE BASE METHOD: Dashboard ***');
    
    # Stash the controller name
	$c->stash->{ controller } = 'Admin::Dashboard';
	
	# Set the Wrapper for the Controller
	$c->stash->{ wrapper } = $c->config->{wrapper}->{'admin'};

    # Load status messages
    $c->load_status_msgs;
}


sub index :Chained('base') :PathPart('') :Args(0) {
    my ($self, $c) = @_;
    
	# Check to make sure user has the Access to Dashboard
	if ($c->user_exists() && $c->check_any_user_role( qw/ superuser admin mgmt staff /) ) {
		
	} else {
        $c->detach('Root', 'error_noperms')
    }


}


sub dashboard :Chained('base') :PathPart('dashboard') :Args(0) {
    my ($self, $c) = @_;
    
	# Check to make sure user has the Access to Dashboard
	if ($c->user_exists() && $c->check_any_user_role( qw/ superuser admin mgmt staff /) ) {
		
	} else {
        $c->detach('Root', 'error_noperms')
    }


}

__PACKAGE__->meta->make_immutable;

1;
