package GKS::Controller::Root;
use Moose;
use namespace::autoclean;

use GKS::Form::User::Signin;

BEGIN { extends 'GKS::Controller' }


#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

# Note that 'auto' runs after 'begin' but before your actions and that
# 'auto's "chain" (all from application path to most specific class are run)
# See the 'Actions' section of 'Catalyst::Manual::Intro' for more info.
sub auto :Private {
    my ($self, $c) = @_;

    # Allow unauthenticated users to reach the login page.  This
    # allows unauthenticated users to reach any action in the Login
    # controller.  To lock it down to a single action, we could use:
    #   if ($c->action eq $c->controller('Login')->action_for('index'))
    # to only allow unauthenticated access to the 'index' action we
    # added above.
#    if ($c->controller eq $c->controller('Login') ||
#    	$c->controller eq $c->controller('Root') ||
#    	($c->action eq $c->controller('User')->action_for('signup')) ||
#    	($c->action eq $c->controller('User')->action_for('signup_do'))
#    	
#    	) {
#        return 1;
#    }

	# If a user doesn't exist, force login
#	if (!$c->user_exists) {
#	    # Dump a log message to the development server debug output
#	    $c->log->debug('***Root::auto User not found, forwarding to /login');
#	    # Redirect the user to the login page
#	    $c->response->redirect($c->uri_for('/login'));
#	    # Return 0 to cancel 'post-auto' processing and prevent use of application
#	    $c->flash->{redirect_after_login} = '' . $c->req->uri;  # this is new
#	    return 0;
#	}

    # User found, so return 1 to continue with processing after this 'auto'    
    
    ##Set the Template Wrapper for the site
    my $system = 'default';

    my $req = $c->request;
    if($req->uri->host =~ 'students'){
    	$system = 'students';
    }
    
    $c->stash->{ homepage } = $c->config->{$system}->{homepage};
    $c->stash->{ systitle } = $c->config->{$system}->{systitle};
	$c->stash->{ wrapper } = $c->config->{wrapper}->{$system};

    return 1;
}

=head2 base

Stash top-level config items, check for affiliate ID and set cookie if found

=cut

sub base : Chained( '/' ) : PathPart( '' ) : CaptureArgs( 0 ) {
    my ( $self, $c ) = @_;
	
#	my $now = DateTime->now;
#    $c->stash(
#        recaptcha_public_key  => $self->recaptcha_public_key,
#        recaptcha_private_key => $self->recaptcha_private_key,
#        upload_dir            => $self->upload_dir,
#        now                   => $now,
#    );
#    
#    if ( $c->request->param( 'affiliate' ) ) {
#		$c->response->cookies->{ shinycms_affiliate } = 
#			{ value => $c->request->param( 'affiliate' ) };
#    }
}



=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
    my $homepage = $c->stash->{'homepage'};
    my $wrapper = $c->stash->{'wrapper'};
    
    if ( $c->user_exists ) {
		#$c->response->redirect( $c->uri_for('/admin/dashboard') );
		#return;
		$c->detach('User', '_redirect_after_signin');
	}
    

#	if($c->config->{'staff_system'}) {
#		# This is the Staff System
#		# Redirect to admin login
#		$c->go( 'Admin::User', 'login' );
#		return;
#	}

	my $form = GKS::Form::User::Signin->new(name =>'loginform', action=> $c->uri_for('/user/signin'));
    #Load Controller specific CSS/JS
	#$c->stash->{cust_files} = {css =>['/static/css/example.css'], js =>['/static/js/validation/login.js']};
	#$c->stash->{wrapper} = 'frontend_wrapper.tt2';
    $c->stash(template => $homepage, wrapper => $wrapper, form => $form,);
}


=head2 admin

Forward to the admin area

=cut

#sub admin : Path( 'admin' ) : Args( 0 ) {
#	my ( $self, $c ) = @_;
#	
#	# Redirect to admin login
#	$c->detach( 'Admin::Dashboard', 'index' );
#}


=head2 login

Forward to the user-facing login

=cut

sub login : Path( 'login' ) : Args( 0 ) {
	my ( $self, $c ) = @_;
	
	# Redirect to user login
	$c->go( 'User', 'signin' );
}



=head2 error_noperms

Permissions error screen

=cut

sub error_noperms :Chained('/') :PathPart('error_noperms') :Args(0) {
    my ($self, $c) = @_;

    $c->stash(template => 'error_noperms.tt2');
}


=head2 default

Standard 404 error page

=cut

sub default :Path {
    my ( $self, $c ) = @_;
    #$c->response->body( 'Page not found' );
    $c->response->status(404);
    $c->stash(template => '404.tt2', no_wrapper => 1);
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : ActionClass('RenderView') {
    my ($self, $c) = @_;
    my $errors = scalar @{$c->error};
#    if ($errors) {
#        $c->res->status(500);
#        $c->res->body('internal server error');
#        $c->clear_errors;
#    }
}


__PACKAGE__->meta->make_immutable;

1;
