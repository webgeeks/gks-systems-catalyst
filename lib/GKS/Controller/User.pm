package GKS::Controller::User;
use Moose;
use utf8;
use Data::UUID;
use DateTime;
use DateTime::Format::Strptime;
use namespace::autoclean;
use Data::Dumper;
use Captcha::reCAPTCHA;

use GKS::Form::User::ChangePassword;
#use GKS::Form::User::Account;
#use GKS::Form::User::NewAccount;
#use GKS::Form::User::Signup;
use GKS::Form::User::Signin;

BEGIN { extends 'Catalyst::Controller'; }


sub base :Chained('/') :PathPart('user') :CaptureArgs(0) {
    my ($self, $c) = @_;

    # Store the ResultSet in stash so it's available for other methods
    $c->stash(user_rs => $c->model('DB::User'));

    # Print a message to the debug log
    $c->log->debug('*** INSIDE BASE METHOD: User ***');

    # Load status messages
    $c->load_status_msgs;

}


#sub __signup :Chained('base') :PathPart('signup') :Args(0) {
#    my ($self, $c) = @_;
#	#my $req = $c->request;
#	my $schema = $c->model('DB')->schema;
#	my $signup = $c->model('DB::Account')->new_result({});
#	#my $accounts = $c->model('DB::Accounts');
#	my $user = $c->model('DB::User');
#	$ENV{REMOTE_ADDR} = $c->req->address;
#	my $form = GKS::Form::User::Signup->new;
#	
#	# Data::UUID seems to generate better (less predictable) UUID's than MySQL
#    my $ug    = new Data::UUID;
#  	my $uuid = $ug->create_str();
#  	my $user_uuid = $ug->create_str();
#  	#$form->{users}->{user_uuid} = $user_uuid;
#  	#$form->account_uuid($uuid);
#    #$form->user_uuid($user_uuid);
#    #$form->{users}->{username}($c->request->params->{email_address});
#    my $defaults = {
#        'users.user_uuid' => $user_uuid,
#        'users.created_by' => 'Sign-Up',
#        'users.is_primary' => '1',
#
#   };
#   #defaults => $defaults
#	#$c->log->debug('REMOTE_ADDR: ' . $c->req->address);
#	#print Dumper(%ENV);
#	#my $role = $signup->update_or_create( 'user_roles', {role_id => '5'});
#    #$c->log->debug('Role: ' . $role);
#    #$c->log->debug('*****************************************\n*************************');      
#    
#	#$form->process( item => $signup, params => $c->req->body_parameters, defaults => $defaults, action => $c->uri_for('signup_do') );
#	$form->process(posted => ($c->req->method eq 'POST'), params => $c->request->body_parameters);        
#
#    # Set the TT template to use
#    #$c->stash(form => $form, template => 'user/signup.tt2');
#    #$c->log->debug('reCAPTCHA: ' . $form->ran_validation);
#    if($form->ran_validation){
#    	
#	
#	    # Verify username doesn't already exist
#	    my $username = $c->request->params->{email_address};
#	    my $count = $c->model('DB::User')->count({username => $username});	
#	    if ( $count > 0 ) {
#	        #print "Yes!";
#	        #$c->log->debug('reCAPTCHA: Yes!');
#	        $form->add_form_error("<div> - User already exists in the system.</div>");
#	    }
#    }
#    
#    if($form->validated){
#    	# Create Account, User and related records
#    	my $ug    = new Data::UUID;
#	  	my $uuid = $ug->create_str();
#	  	my $user_uuid = $ug->create_str();
#	  	my $username = $c->request->params->{email_address};
#    	my $password = $c->request->params->{new_password};
#	  	# Create the Account
#	    my $account = $c->model('DB::Account')->create({
#	            account_uuid   => $uuid,
#	            status  => '1',
#	        });
#		#print Dumper($account->get_column('idaccounts'));
#	        
#	    my $user = $c->model('DB::User')->create({
#	            user_uuid   => $user_uuid,
#	            accounts_idaccounts => $account->get_column('idaccounts'),
#	            status  => '1',
#	            username => $username,
#	            email_address => $c->request->params->{email_address}, 
#	            password  => $password,
#	        });
#        $user->create_related('user_roles', {role_id => '5'});
#        
#        $user->create_related('profiles', {type    => '10', status  => '1',
#        		first_name => $c->request->params->{'first_name'},
#        		last_name => $c->request->params->{'last_name'},
#        	
#        });
#	  	
#	  	if ($c->authenticate({ username => $username,
#	                       password => $password} )) {
#	        my $user = $c->user();
#	        $user->update({last_log_in => \'NOW()'});
#	        # If successful, then let them use the application
#	        # Need business logic here for default modules, will redirect to dashboard for now.
#	        # Check to see if subscription expired and redirect - ELSE Dashboard
#	        $c->response->redirect($c->uri_for('/dashboard'));
#	        #$c->response->redirect($redirect) if ($redirect);  # this is new
#	        return;
#	    } else {
#	        # Set an error message
#	        #$c->stash->{error_msg} = "Bad username or password.";
#	        $form->add_form_error("Unable to log user into system.");
#	    }
#	  	
##	    $c->stash(
##	    			#account  => $account, 
##	    			#user => $user,
##	              	template => 'user/create_done.tt2');
#    } else {
#    	
#    	$c->stash(form => $form, template => 'user/signup.tt2', wrapper => 'frontend_wrapper.tt2');
#    }
#}
#
#
#sub __signup_do :Chained('base') :PathPart('signup_do') :Args(0) {
#	my ($self, $c) = @_;
#	
#	my $ug    = new Data::UUID;
#  	my $uuid = $ug->create_str();
#  	my $user_uuid = $ug->create_str();
#	
#	# Retrieve the values from the form
#    my $title     = $c->request->params->{title}     || 'N/A';
#    my $rating    = $c->request->params->{rating}    || 'N/A';
#    my $author_id = $c->request->params->{author_id} || '1';
#
#    # Create the book
#    my $account = $c->model('DB::Account')->create({
#            account_uuid   => $uuid,
#            status  => '1',
#        });
#	#print Dumper($account->get_column('idaccounts'));
#        
#    my $user = $c->model('DB::User')->create({
#            user_uuid   => $user_uuid,
#            accounts_idaccounts => $account->get_column('idaccounts'),
#            status  => '1',
#        });
#        
#        $user->create_related('profiles', {type    => '10', status  => '1',});
#        
##    my $user =   $account->create_related('users', {user_uuid => $user_uuid});
##    
##    print Dumper($user->{'_column_data'}->{'id'});
##    
##    my $profile = $c->model('DB::Profile')->create({
##    		users_id => $user->{'_column_data'}->{'id'},
##    		type    => '10',
##    		status  => '1',    	
##    });
#    
#    # Handle relationship with author
#    #$book->add_to_book_authors({author_id => $author_id});
#    # Note: Above is a shortcut for this:
#    # $book->create_related('book_authors', {author_id => $author_id});
#    
#    $c->stash(account  => $account, 
#    			#user => $user,
#              template => 'user/create_done.tt2');
#
#}

sub change_password : Chained('base') :PathPart('change_password') :Args(0) {
    my ($self, $c) = @_;
	my $current_password = $c->request->params->{current_password};
    my $form = GKS::Form::User::ChangePassword->new();
	$form->process(posted => ($c->req->method eq 'POST'), params => $c->request->body_parameters);

	if($c->request->params->{current_password} && $form->validated){
		if($c->user->check_password($current_password)) {
			$c->log->debug('***Users::change_password: Password OK');
			$c->user->update({
        			password  => $c->request->params->{'new_password'},
    				});
    		    $c->response->redirect($c->uri_for('/dashboard', {
        			mid => $c->set_status_msg("Password Successfully Updated."),
    				}));
		} else {
			$form->add_form_error("Incorrect current password.");
			$c->log->debug('***Users::change_password: WRONG!');
		}
	}

    $c->stash(form => $form, template => 'user/change_password.tt2');
}

#sub __admin : Chained('base') :PathPart('admin') :CaptureArgs(0) {
#	 my ($self, $c) = @_;
#	## Placeholder for Roles check
#	$c->detach('/error_noperms')
#        	unless $c->check_any_user_role(qw/superuser admin mgmt staff/);
#}

#sub object :Chained('admin') :PathPart('account') :CaptureArgs(1) {
#    my ($self, $c, $uuid) = @_;
#    # Create our Account object
#	# Store the ResultSet in stash so it's available for other methods
#    $c->stash(resultset => $c->model('DB::Account'));
#    # Find the book object and store it in the stash
#    $c->stash(object => $c->stash->{resultset}->find({account_uuid => $uuid}));
#
#    # Make sure the lookup was successful.  You would probably
#    # want to do something like this in a real app:
#    #   $c->detach('/error_404') if !$c->stash->{object};
#    die "UID $uuid not found!" if !$c->stash->{object};
#
#    # Print a message to the debug log
#    #$c->log->debug("*** INSIDE OBJECT METHOD for obj id=$id ***");
#}

#sub __account_new : Chained('admin') :PathPart("account_new") :Args(0) {
#    my ($self, $c) = @_;
#    my $schema = $c->model('DB')->schema;
#    my $account = $c->model('DB::Account')->new_result({});
#
#    #$c->log->debug('Account Name: ' . $row->account_name);
#    my $form = GKS::Form::User::NewAccount->new(schema => $schema,);
#    #$form->lookup_options(active_column => 'active' );
#    # Data::UUID seems to generate better (less predictable) UUID's than MySQL
#    my $ug    = new Data::UUID;
#  	my $uuid = $ug->create_str();
#    $form->created( $c->datetime());
#    $c->stash(form => $form, template => 'user/new_account.tt2');
#    $form->account_uuid($uuid);
#    #$c->log->debug('ActiveCol: ' . $active_col);
#	return unless $form->process(posted => ($c->req->method eq 'POST'),
#							item => $account,
#							params => $c->request->body_parameters);
#	# my $new_id = $c->stash->{form}->item->id;
#
#	$c->response->redirect( $c->uri_for($self->action_for('list_accounts')) );
#
#}
#
#sub __admin_account : Chained('admin') :PathPart("account") :Args(0) {
#    my ($self, $c) = @_;
#    my $schema = $c->model('DB')->schema;
#    my $accountid = $c->model('DB::UserAccount')->find({users_id => $c->user->id});
#    my $row = $c->model('DB::Account')->find({idaccounts => $accountid->accounts_id});
#    #$c->log->debug('DateTime: ' . $row->created);
#    my $form = GKS::Form::User::Account->new(schema => $schema, form_wrapper_class => ['span4']);
#	$form->process(posted => ($c->req->method eq 'POST'),
#							item => $row,
#							params => $c->request->body_parameters);
#
#    $c->stash(form => $form, template => 'user/account.tt2');
#}

#sub __account_edit :Chained('object') :PathPart('account_edit') :Args(0) {
#    my ($self, $c) = @_;
#
#	my $ug    = new Data::UUID;
#  	my $uuid1 = $ug->create_str();
#  	$c->stash->{'UUID'} = $uuid1;
#    # Saved the PK id for status_msg below
#    my $accountid = $c->stash->{object}->id;
#    my $schema = $c->model('DB')->schema;
#    #my $accountid = $c->model('DB::UserAccount')->find({users_id => $c->user->id});
#    my $row = $c->model('DB::Account')->find({idaccounts => $accountid});
#    #$c->log->debug('DateTime: ' . &date_format_mmddyyyy($row->created));
#    my $form = GKS::Form::User::Account->new(schema => $schema, wrapper_attributes =>{class =>'foobar' });
#    
#	$form->process(posted => ($c->req->method eq 'POST'),
#							item => $row,
#							params => $c->request->body_parameters);
#
#    $c->stash(form => $form, template => 'user/account.tt2', account_uuid => $row->account_uuid);
#
#}
#
#
#sub __list_users :Chained('object') :PathPart('users') :Args(0) {
#    my ($self, $c) = @_;
#
#    my $account_uuid = $c->stash->{object}->id;
#    #my $schema = $c->model('DB')->schema;
#    #my $row = $c->model('DB::Users')->find({idaccounts => $accountid});
#    #$c->log->debug('DateTime: ' . &date_format_mmddyyyy($row->created));
#    #$c->stash(accounts => [$c->model('DB::Account')->search({}, {order_by => 'account_name'})]);
#    $c->stash(users => [$c->model('DB::User')->users_list($account_uuid)]);
#    $c->stash(template => 'user/users_list.tt2', account_uuid => $c->stash->{object}->account_uuid );
#}


sub signin : Chained('base') : PathPart('signin') : Args(0) {
	my ( $self, $c ) = @_;

	# Load status messages
	$c->load_status_msgs;

	if ( $c->user_exists ) {
		#$c->response->redirect( $c->uri_for('/admin/dashboard') );
		#return;
		$c->detach('_redirect_after_signin');
	}

	# Get the username and password from form
	my $username = $c->request->params->{username};
	my $password = $c->request->params->{password};
	my $redirect;
	#$c->keep_flash( qw(redirect_after_login) ); # Need to save this till we're done.
	
#my $form = GKS::Form::User::Login->new(form_element_class =>['hfh', 'admin'], name =>'login', style=>'width:300px;');
	my $form = GKS::Form::User::Signin->new(
		name   => 'loginform',
		action => $c->uri_for('user/signin')
	);

	#$form->to_jquery_validation_profile();
	$form->process( );

	# If the username and password values were found in form
	if ( $username && $password ) {

		# Attempt to log the user in
		if (
			$c->authenticate(
				{
					username => $username,
					password => $password
				}
			)
		  )
		{
			my $user  = $c->user();
			#my $ipnum = $self->logIP($c);
			$user->create_related(
				'user_logins',
				{
					last_log_in    => $c->datetime->datetime(),
					log_in_success => 1,
					log_in_ip      => $c->model('Utils')->getip2num($c->request->address),
					user_agent	   => $c->request->user_agent,
					authen_src	   => $c->user->auth_realm
				}
			);
			#Get the User's Name and Stash it for later...
			my $currentuser = $c->model('DB::UserProfile')->find({users_id => $c->user->id, is_primary => 1});
			$c->session->{current_user}{first_name} = $currentuser->get_column('first_name');
			$c->session->{current_user}{last_name} = $currentuser->get_column('last_name');
			#$c->log->debug('Name: ' . $currentuser->get_column('last_name') );

			# If successful, then let them use the application
			# Need business logic here for default modules, will redirect to dashboard for now.
			# Check to see if subscription expired and redirect - ELSE Dashboard

			$c->detach('_redirect_after_signin');
			return;
		}
		else {

			# Set an error message
			#$c->stash->{error_msg} = "Bad username or password.";
			$form->add_form_error("Incorrect username or password.");
		}
	}

	#$form->form_element_class(['hfh', 'admin']);
	#Load Controller specific CSS/JS
	$c->stash->{cust_files} =
	  { css => [], js => ['/static/js/validation/login.js'] };

	# If either of above don't work out, send to the login page
	$c->stash(
		#template   => '/user/signin.tt2',
		form       => $form,
		#no_wrapper => 1
	);
}




=head2 logout

Logout logic.

=cut

sub signout : Chained( 'base' ) : PathPart( 'signout' ) : Args( 0 ) {
	my ( $self, $c ) = @_;
	
	# Clear the user's session
	$c->logout;
	
	# Set a status message
	$c->stash->{ status_msg } = 'You have been logged out.';
	
	# Send the user to the site's homepage
	$c->response->redirect( $c->uri_for( '/' ) );
}

sub validuser : Chained('base') : PathPart('ValidUser') : Args(0) {
	my ( $self, $c ) = @_;
	$c->stash->{no_wrapper} = 1;
	
	if ($c->request->referer =~ /lovegevity/){
		# Let's try to prevent brute force guessing usernames
		# Restrict requests to referer with lovegevity in the name
	# Verify username doesn't already exist
	my $username = $c->request->params->{'username'};
	if ( $c->model('DB::User')->user_exists($username) > 0 ) {

		$c->response->body('<i class="fa fa-exclamation text-danger"></i><span class="text-danger"> User already exists in the system.</span>');
	}
	else {
		$c->response->body('<i class="fa fa-check text-success"></i><span class="text-success"> Username Available</span>');
	}
	}

}


sub reset_password :Path('reset_password') :Args(0) {
    my ($self, $c) = @_;
    my $username = $c->request->params->{email_address};
    my $form = GKS::Form::User::ResetPassword->new();
	$form->process(posted => ($c->req->method eq 'POST'), params => $c->request->body_parameters);
	
	my $user = $c->model('DB::User');
	my $count = $user->count({username => $username});

	#print Dumper($c->datetime->datetime());
	
#	if($count == 1) {
#	# User found, send email
#	my $uid    = new Data::UUID;
#  	my $token = $uid->create_str();
##  	my $expires = $c->model('DB::User')->dt_datetime->datetime(); 
#  	$user->update({
#  		forgot_passwd_token => $token,
#  		forgot_passwd_token_expires => $c->datetime->datetime(),
#  		{username => $username}  		
#  	});
#	$c->stash->{email} = {
#            to      => 'mdmaynard42@the-web-geeks.com',
#            #cc      => 'abraxxa@cpan.org',
#            from    => 'no-reply@foobar.com',
#            subject => 'I am a Catalyst generated email',
#            body    => 'Body Body Body',
#        };
#        
#        $c->forward( $c->view('Email') );
#	} else {
#	# User not found, display error
#		
#	}

    $c->stash(form => $form, template => 'user/reset_password.tt2', wrapper => 'frontend_wrapper.tt2' );
}

sub _redirect_after_signin : Private {
	my ( $self, $c ) = @_;
	
	if($c->check_any_user_role( qw/superuser admin mgmt staff/ )) {
		$c->session->{wrapper} = 'admin';
		$c->response->redirect( $c->uri_for('/admin/dashboard') );
	} elsif ($c->check_any_user_role( qw/student/ )) {
		$c->session->{wrapper} = 'student';
		$c->response->redirect( $c->uri_for('/student/dashboard') );
	} else {
		$c->response->redirect( $c->uri_for('/error_noperms') );
	}
	return 0;
}

#sub date_format_mmddyyyy{
#	my ( $datetoformat ) = @_;
#      #2013-08-21T00:00:00
#      #Expected Format: %Y-%m-%d %H:%M:%S
#
#    my $strp = DateTime::Format::Strptime->new(
#        pattern   => '%Y-%m-%dT%H:%M:%S',
#        on_error  => 'croak',
#    	);
#    my $dt = $strp->parse_datetime($datetoformat);
#    my $format = $dt->strftime('%m-%d-%Y');
#    return $format;
#}

__PACKAGE__->meta->make_immutable;

1;
