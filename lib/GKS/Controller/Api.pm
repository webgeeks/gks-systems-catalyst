package GKS::Controller::Api;
use Moose;
use namespace::autoclean;
BEGIN { extends 'GKS::Controller' }
use JSON::XS;
use XML::Simple;
 
sub api : Chained('/') PathPart('api') CaptureArgs(0) {
    my ( $self, $c ) = @_;
 
    if (!exists($c->stash->{'api_params'})) {
        $c->stash->{'api_params'} = $c->req->params;
        if (!exists($c->stash->{'api_params'}{'output'})) {
            $c->stash->{'api_params'}{'output'} = 'json';
        }
    } elsif (!exists($c->stash->{'api_params'}{'output'})) {
        $c->stash->{'api_params'}{'output'} = 'json';
    }
    ## This sets the default response as a failure.  
    $c->stash->{apiresponse} = { 'processed' => 0, 'status' => 'failed' };
 
    ## this part is optional - if you don't need any kind of authentication, you
    ## can disable this. 
#    if (!$c->stash->{'api_params'}{'authkey'} || 
#         $c->stash->{'api_params'}{'authkey'} ne $c->config->{'notifications'}{'authkey'}) {
# 
#        # we fail authentication, so we dump them out to the auth-failed action
#        $c->stash->{'apiresponse'} = { 'processed' => 0, 'error' => { 'general' => 'service not available' }};
#        $c->detach();
#    }
}
 
sub gettime : Chained('api') PathPart('gettime') Args(0){
   my ($self, $c) = @_;
 
   # do something interesting.
   my $server_time = scalar localtime;
 
   $c->stash->{apiresponse} = { 
                                'processed' => 1, 
                                'status' => 'OK', 
                                'server_time' => $server_time,
                              };
 
}
 
sub end : Private {
    my ( $self, $c ) = @_;
 
    if ($c->stash->{'api_params'}{'output'} eq 'xml') 
    {
         my $xml = XML::Simple->new( NoAttr => 1, RootName => 'apiresponse', XMLDecl => 1);
         $c->response->body($xml->XMLout($c->stash->{'apiresponse'}));
    } else {
        my $jsonobject = JSON::XS->new->utf8->pretty(1);
        my $responsetext = $jsonobject->encode($c->stash->{'apiresponse'});
        $c->response->body($responsetext);
    }
}

__PACKAGE__->meta->make_immutable;

1;