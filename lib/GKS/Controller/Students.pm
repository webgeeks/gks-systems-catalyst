package GKS::Controller::Students;
use Moose;
use namespace::autoclean;

#use DateTime;
use Data::Dumper;
use Data::UUID;
use Digest::MD5 qw(md5_hex);

use HTTP::Response;
use WWW::Curl::Easy;
use XML::Simple;

# Load our Forms
#use GKS::Form::User::Account;
#use GKS::Form::User::ChangePassword;
#use GKS::Form::User::Login;
#use GKS::Form::User::NewProfile;


BEGIN { extends 'GKS::Controller'; }

sub base : Chained('/base') PathPart('students') CaptureArgs(0){
    my ($self, $c) = @_;

	#my $ymid = gen_ym_callid();
	#$c->log->debug('*** CallID: ' . $ymid );
	
## Get/Set/Ping Current YM Sesion

	my $ymsessionid = $c->session->{'YMSessionID'};
	my ($xml, $xsout, $apicall, $xs, $ym);
	my $returnurl = $c->uri_for('/students');	
	
	if($c->session->{'YMSessionID'}){
		
		my $sessionping = {
	  		Version => [ $c->config->{YM_API}->{ver} ],
	  		ApiKey	=> [ $c->config->{YM_API}->{key} ],
	  		CallID	=> [ $c->model('UUID')->gen_callid ],
	  		SessionID	=> [ $ymsessionid ],
	  		Call 	=> {
	  			Method	=> "Session.Ping",
	  		},
	  	};
  	 $c->log->debug('*** Session.Ping ***');
  		$xsout = XML::Simple->new();
  		$apicall = $xsout->XMLout($sessionping, RootName => 'YourMembership', NoSort => 1,  XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>');

		$xml = sendxmlrequest($c->config->{YM_API}, $apicall);		
		$xs = XML::Simple->new();	
	    $ym = $xs->XMLin($xml);	
	    
	    if($ym->{'Session.Ping'} == 1){
		#Session good	    	
	    } else {
	    #Need new session
	    	delete $c->session->{'YMSessionID'};
	    }		
	}
	
	unless($c->session->{'YMSessionID'}){
		$c->log->debug('*** Session.Create ***');
		my $sessioncreate = {
	  		Version => [ $c->config->{YM_API}->{ver} ],
	  		ApiKey	=> [ $c->config->{YM_API}->{key} ],
	  		CallID	=> [ $c->model('UUID')->gen_callid ],
	  		Call 	=> {
	  			Method	=> "Session.Create",
	  		},
	  	};
  
  		$xsout = XML::Simple->new();
  		$apicall = $xsout->XMLout($sessioncreate, RootName => 'YourMembership', NoSort => 1,  XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>');
  
		$xml = sendxmlrequest($c->config->{YM_API}, $apicall);		
		$xs = XML::Simple->new();	
	    $ym = $xs->XMLin($xml);	
		#print $ym->{'Session.Create'}{'SessionID'} . "\n\n";
		$c->session->{'YMSessionID'} = $ym->{'Session.Create'}{'SessionID'};
		
	}

    # Print a message to the debug log
    #$c->log->debug('*** INSIDE BASE METHOD: Dashboard ***');
    
    # Stash the controller name
	$c->stash->{ controller } = 'Students';
	
	# Set the Wrapper for the Controller
	$c->stash->{ wrapper } = $c->config->{wrapper}->{'frontend'};

    # Load status messages
    $c->load_status_msgs;
}


sub index :Chained('base') :PathPart('') :Args(0) {
    my ($self, $c) = @_;
    
	# Check to make sure user has the Access to Dashboard
	my @roles = qw(superuser admin mgmt staff student);
	my $redirect_to_login = $c->uri_for( '/admin/user/login' );
	my $redirect_after_login = $c->req->uri;
	return 0 unless $self->AuthUserRole($c, {
		action => 'Student Admin',
		role   => \@roles, 
		redirect => $redirect_to_login,
		redirect_after_login => $redirect_after_login
	});

}


sub ym_is_auth :Chained('base') :PathPart('ymauth') :Args(0) {
    my ($self, $c) = @_;
    

}


sub add_vendor :Chained('base') :PathPart('vendoradd') :Args(0) {
    my ($self, $c) = @_;
    

}

sub ymlogin :Chained('base') PathPart('ymlogin') Args(0){
	my ($self, $c) = @_;

	my $ymsessionid = $c->session->{'YMSessionID'};
	my ($xml, $apicall);
	my $returnurl = $c->uri_for('/students/dashboard');
	
	$c->log->debug('***' . $returnurl . '***');
	
	my $createtoken = {
  		Version => [ $c->config->{YM_API}->{ver} ],
  		ApiKey	=> [ $c->config->{YM_API}->{key} ],
  		CallID	=> [ $c->model('UUID')->gen_callid ],
  		SessionID	=> [ $ymsessionid ],
  		Call 	=> {
  			Method	=> "Auth.CreateToken",
  			Persist => [ '1' ],
  			RetUrl => [ "$returnurl" ],
  		},
  	};
		
	
	my $xsout = XML::Simple->new();
  	   $apicall = $xsout->XMLout($createtoken, RootName => 'YourMembership', NoSort => 1,  XMLDecl => '<?xml version="1.0" encoding="UTF-8"?>');

	my $authtoken = sendxmlrequest($c->config->{YM_API}, $apicall);	

	my $ymxmltoken = XML::Simple->new();
	my $tokenxml = $ymxmltoken->XMLin($authtoken);
	my $redirect;
	$c->log->debug('***' . $tokenxml->{'Auth.CreateToken'}{'GoToUrl'} . '***');
	if($tokenxml->{'Auth.CreateToken'}{'GoToUrl'}) {
		$redirect = $tokenxml->{'Auth.CreateToken'}{'GoToUrl'};
	} else {
		$c->session->{'YMAuthToken'} = $tokenxml->{'Auth.CreateToken'}{'AuthToken'};
		$redirect = $tokenxml->{'Auth.CreateToken'}{'RetUrl'};
	}
		
	$c->response->redirect( $redirect );
	return 0;	
	
}


sub sendxmlrequest() {
	my ($ym, $xmlrequest) = @_;
	my $url = $ym->{url};
	my $response;
	my $ymapi;
	
    my $curl = WWW::Curl::Easy->new;
    $curl->setopt(CURLOPT_VERBOSE() , 1);
    $curl->setopt(CURLOPT_HEADER(), 0);
    $curl->setopt(CURLOPT_URL(), $url);
    $curl->setopt(CURLOPT_HTTPHEADER(), ['Content-Type: application/x-www-form-urlencoded']);
    $curl->setopt(CURLOPT_POST(), 1);
    $curl->setopt(CURLOPT_POSTFIELDS(), $xmlrequest);

    $curl->setopt(CURLOPT_WRITEDATA(), \$response);

    my $retcode = $curl->perform;
    if (0 == $retcode) {
        $response = HTTP::Response->parse($response);
        $ymapi = $response->decoded_content;
    } else {
        die sprintf 'libcurl error %d (%s): %s', $retcode, $curl->strerror($retcode), $curl->errbuf;
    }
    
    return $ymapi;
}

#	$c->log->debug('XML: ' . $sessioncreate);
#	$c->log->debug('Response: ' . $xml);


#sub gen_callid() {
#    my $ug = new Data::UUID;
#    return substr($ug->create_hex(), 0, 20);
#}


__PACKAGE__->meta->make_immutable;

1;
